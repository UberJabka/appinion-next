import {
    Schema,
    model,
    Document,
    PassportLocalSchema,
    PassportLocalModel,
} from 'mongoose';
import * as mongoose from 'mongoose';

delete mongoose.connection.models.Shop;

export interface IShop extends Document {
    name: string;
    desc?: string;
    userId: string;
    script?: string;
    photo?: {
        url?: string;
    };
}

const ShopSchema: PassportLocalSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    desc: {
        type: String,
    },
    userId: {
        type: String,
        required: true,
    },
    photo: {
        url: {
            type: String,
        },
    },
});

export default model('Shop', ShopSchema) as PassportLocalModel<IShop>;
