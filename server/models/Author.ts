import { Schema } from 'mongoose';

const AuthorSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    location: {
        type: String,
    },
    photo: {
        url: {
            type: String,
        },
    },
});

export interface IAuthor {
    name: string;
    location?: string;
    photo?: {
        url: string;
    };
}

export default AuthorSchema;
