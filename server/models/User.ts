import {
    Schema,
    model,
    Document,
    PassportLocalSchema,
    PassportLocalModel,
    PassportLocalOptions,
} from 'mongoose';
import * as mongoose from 'mongoose';
import passportLocalMongoose from 'passport-local-mongoose';

delete mongoose.connection.models.User;

export interface IUser extends Document {
    id: string;
    email: string;
    firstName: string;
    lastName: string;
    phone: string;
    country?: string;
    city?: string;
    photo?: {
        url?: string;
    };
}

const UserSchema: PassportLocalSchema = new Schema({
    id: {
        type: String,
        required: true,
    },
    firstName: {
        type: String,
        required: true,
    },
    lastName: {
        type: String,
        required: true,
    },
    phone: {
        type: String,
    },
    country: {
        type: String,
    },
    city: {
        type: String,
    },
    photo: {
        url: {
            type: String,
        },
    },
});

UserSchema.plugin(passportLocalMongoose, {
    usernameField: 'email',
} as PassportLocalOptions);

export default model('User', UserSchema) as PassportLocalModel<IUser>;
