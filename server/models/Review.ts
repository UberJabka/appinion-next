import { Schema, Document, model } from 'mongoose';
import * as mongoose from 'mongoose';
import AuthorSchema, { IAuthor } from './Author';

delete mongoose.connection.models.Review;

const ReviewSchema = new Schema(
    {
        shopId: {
            type: String,
        },
        author: AuthorSchema,
        value: {
            type: String,
        },
        pros: {
            type: String,
        },
        cons: {
            type: String,
        },
        audio: {
            url: {
                type: String,
            },
        },
        media: [
            {
                url: {
                    type: String,
                },
            },
        ],
        rating: {
            type: Number,
            enum: [1, 2, 3, 4, 5],
        },
        reactions: [
            {
                label: {
                    type: String,
                    enum: ['LIKE', 'DISLIKE'],
                },
                count: {
                    type: Number,
                },
            },
        ],
    },
    {
        timestamps: true,
    },
);

enum Reactions {
    LIKE,
    DISLIKE,
}

export interface IReview extends Document {
    shopId?: string;
    date?: string;
    author: IAuthor;
    value?: string;
    pros?: string;
    cons?: string;
    audio?: {
        url: string;
    };
    media?: [
        {
            url: string;
        },
    ];
    rating: number;
    reactions?: [
        {
            label: Reactions;
            count: number;
        },
    ];
}

export default model<IReview>('Review', ReviewSchema);
