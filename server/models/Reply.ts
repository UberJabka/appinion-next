import mongoose, { Schema, Document, model } from 'mongoose';
import AuthorSchema, { IAuthor } from './Author';

delete mongoose.connection.models.Reply;

const ReplySchema = new Schema(
    {
        reviewId: { type: Schema.Types.ObjectId, ref: 'Review' },
        author: AuthorSchema,
        value: String,
    },
    { timestamps: true },
);

export interface IReply extends Document {
    reviewId: string;
    author: IAuthor;
    value: string;
    date?: string;
}

export default model<IReply>('Reply', ReplySchema);
