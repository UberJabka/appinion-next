import * as mongoose from 'mongoose';

const configDB = () => {
    if (!mongoose.connection.readyState) {
        mongoose.connect(process.env.DB_HOST, {
            useNewUrlParser: true,
        });
    }
};

export default configDB;
