import passport from 'passport';
// import cookieSession from 'cookie-session';
import url from 'url';
import redirect from 'micro-redirect';
import session from 'express-session';
import mongoStore from 'connect-mongo';
import * as mongoose from 'mongoose';
import User, { IUser } from '../models/User';
import runMiddleware from '../utils/runMiddleware';
import configDB from '../database';

export { default as passport } from 'passport';

const MongoStore = mongoStore(session);

passport.use(User.createStrategy());

export interface PassportSession {
    passport: { user: IUser };
}

// Configure Passport authenticated session persistence.
//
// In order to restore authentication state across HTTP requests, Passport needs
// to serialize users into and deserialize users out of the session.  In a
// production-quality application, this would typically be as simple as
// supplying the user ID when serializing, and querying the user record by ID
// from the database when deserializing.  However, due to the fact that this
// example does not have a database, the complete Github profile is serialized
// and deserialized.
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

const passportInit = runMiddleware(passport.initialize());
const passportSession = runMiddleware(passport.session());

configDB();

// export middleware to wrap api/auth handlers
export default (fn) => async (req, res) => {
    if (!res.redirect) {
        // passport.js needs res.redirect:
        // https://github.com/jaredhanson/passport/blob/1c8ede/lib/middleware/authenticate.js#L261
        // Monkey-patch res.redirect to emulate express.js's res.redirect,
        // since it doesn't exist in micro. default redirect status is 302
        // as it is in express. https://expressjs.com/en/api.html#res.redirect
        res.redirect = (location: string) => redirect(res, 302, location);
    }
    await runMiddleware(
        session({
            secret: 'appinion',
            store: new MongoStore({ mongooseConnection: mongoose.connection }),
        }),
    )(req, res);
    // Initialize Passport and restore authentication state, if any, from the
    // session. This nesting of middleware handlers basically does what app.use(passport.initialize())
    // does in express.
    await passportInit(req, res);

    await passportSession(req, res);

    await fn(req, res);
};
