import { GraphQLUpload } from 'apollo-server-micro';
import { FileUpload } from 'graphql-upload';
import { nanoid } from 'nanoid';
import { createWriteStream, unlink } from 'fs';
import path from 'path';

const storeUpload = async (upload: FileUpload) => {
    const { createReadStream, encoding, filename, mimetype } = await upload;
    const stream = createReadStream();
    const id = nanoid();
    const filePath = `./public/uploads/${id}-${filename}`;
    const file = {
        id,
        filename,
        mimetype,
        path: filePath.replace('./public', ''),
    };

    await new Promise((resolve, reject) => {
        const writeStream = createWriteStream(filePath);

        writeStream.on('finish', resolve);

        writeStream.on('error', (err) =>
            unlink(filePath, () => {
                reject(err);
            }),
        );

        stream.on('error', (error) => writeStream.destroy(error));

        stream.pipe(writeStream);
    });

    return file;
};

export default storeUpload;
