import { ConnectionArguments, cursorToOffset } from 'graphql-relay';

const getOffsetWithDefault = (cursor, defaultOffset) => {
    if (cursor === undefined) {
        return defaultOffset;
    }
    const offset = cursorToOffset(cursor);
    return isNaN(offset) ? defaultOffset : offset;
};

const getOffsetsFromConnectionArgs = (
    args: ConnectionArguments,
    count: number,
) => {
    const { after, before, first, last } = args;

    const beforeOffset = getOffsetWithDefault(before, count);
    const afterOffset = getOffsetWithDefault(after, -1);

    let startOffset = Math.max(-1, afterOffset) + 1;
    let endOffset = Math.min(count, beforeOffset);

    if (first !== undefined) {
        endOffset = Math.min(endOffset, startOffset + first);
    }

    if (last !== undefined) {
        startOffset = Math.max(startOffset, endOffset - last);
    }

    const skip = Math.max(startOffset, 0);
    const limit = endOffset - startOffset;

    return {
        beforeOffset,
        afterOffset,
        startOffset,
        endOffset,
        skip,
        limit,
    };
};

export default getOffsetsFromConnectionArgs;
