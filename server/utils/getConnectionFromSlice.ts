import { ConnectionArguments, offsetToCursor } from 'graphql-relay';
import getOffsetsFromConnectionArgs from './getOffsetsFromConnectionArgs';

const getConnectionFromSlice = (
    inSlice: Array<any>,
    args: ConnectionArguments,
    count: number,
    mapper?: () => any,
) => {
    const { first, after, before, last } = args;
    const {
        startOffset,
        endOffset,
        afterOffset,
        beforeOffset,
    } = getOffsetsFromConnectionArgs(args, count);
    const slice = typeof mapper === 'function' ? inSlice.map(mapper) : inSlice;
    const edges = slice.map((value, index) => ({
        cursor: offsetToCursor(startOffset + index),
        node: value,
    }));

    const firstEdge = edges[0];
    const lastEdge = edges[edges.length - 1];
    const lowerBound = after ? afterOffset + 1 : 0;
    const upperBound = before ? Math.min(beforeOffset, count) : count;

    return {
        edges,
        pageInfo: {
            startCursor: firstEdge ? firstEdge.cursor : null,
            endCursor: lastEdge ? lastEdge.cursor : null,
            hasPreviousPage: last !== null ? startOffset > lowerBound : false,
            hasNextPage: first !== null ? endOffset < upperBound : false,
        },
    };
};

export default getConnectionFromSlice;
