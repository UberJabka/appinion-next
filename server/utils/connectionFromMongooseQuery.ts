import { DocumentQuery } from 'mongoose';
import { ConnectionArguments } from 'graphql-relay';
import getOffsetsFromConnectionArgs from './getOffsetsFromConnectionArgs';
import getConnectionFromSlice from './getConnectionFromSlice';

const connectionFromMongooseQuery = async (
    query: DocumentQuery<any, any>,
    args: ConnectionArguments,
    mapper?: () => any,
) => {
    const count = await query.countDocuments();
    const pagination = getOffsetsFromConnectionArgs(args, count);

    if (pagination.limit === 0) {
        return getConnectionFromSlice([], args, count, mapper);
    }

    query.skip(pagination.skip).limit(pagination.limit).lean();

    const result = await query.find();

    return getConnectionFromSlice(result, args, count, mapper);
};

export default connectionFromMongooseQuery;
