import { MutationUpdaterFn } from '@apollo/client/core';
import { RemoveShopMutation } from '../../src/graphql/generated/graphql';

const updateCache = (
    queryDocument,
    client,
    queryName,
    id,
): MutationUpdaterFn<RemoveShopMutation> => {
    const dataCache = client.readQuery({
        query: queryDocument,
    });
    const newData = {
        queryName: dataCache[queryName].filter((t) => t.id !== id),
    };
    return client.writeQuery({
        query: queryDocument,
        data: newData,
    });
};

export default updateCache;
