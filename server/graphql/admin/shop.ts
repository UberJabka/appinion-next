import {
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
} from 'graphql-relay';
import {
    GraphQLID,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLString,
} from 'graphql';
import { NextApiRequest } from 'next';
import { PassportContext } from 'graphql-passport/lib';
import { IUser } from 'server/models/User';
import { GraphQLUpload } from 'apollo-server-core';
import { FileUpload } from 'graphql-upload';
import { dark } from '@material-ui/core/styles/createPalette';
import Shop from '../../models/Shop';
import { User, Shop as IShop } from '../../../src/graphql/generated/graphql';
import storeUpload from '../../utils/storeUpload';
import ImageType from '../schema/types/ImageType';

export const shopType = new GraphQLObjectType({
    name: 'Shop',
    description: 'A single user object',
    fields: {
        id: globalIdField('Shop', (shop) => shop._id),
        name: { type: GraphQLNonNull(GraphQLString) },
        desc: { type: GraphQLString },
        photo: { type: ImageType },
        userId: { type: GraphQLString },
    },
});

export const addShopMutation = mutationWithClientMutationId({
    name: 'AddShop',
    inputFields: {
        userId: {
            type: GraphQLString,
        },
        name: {
            type: GraphQLNonNull(GraphQLString),
        },
        desc: {
            type: GraphQLString,
        },
        photo: { type: GraphQLUpload },
    },
    outputFields: {
        shop: {
            type: shopType,
            resolve: (payload) => {
                return payload;
            },
        },
    },
    mutateAndGetPayload: async (
        { name, desc, photo },
        ctx: PassportContext<IUser, NextApiRequest>,
    ) => {
        let path = '';
        if (photo) {
            const { filename, mimetype, path: newPath, id } = await storeUpload(
                photo,
            );
            path = newPath;
        }
        const userID = await ctx.getUser()._id;
        return Shop.create({
            name,
            desc,
            photo: {
                url: path,
            },
            userId: userID,
        });
    },
});

export const updateShopMutation = mutationWithClientMutationId({
    name: 'updateShop',
    inputFields: {
        id: { type: GraphQLID },
        name: { type: GraphQLNonNull(GraphQLString) },
        desc: { type: GraphQLString },
        photo: { type: GraphQLUpload },
    },
    outputFields: {
        shop: {
            type: shopType,
            resolve: (payload) => {
                return payload;
            },
        },
    },
    mutateAndGetPayload: async (
        { id, name, desc, photo },
        ctx: PassportContext<User, NextApiRequest>,
    ) => {
        let photoUrl = null;
        if (typeof photo !== 'string') {
            const { filename, mimetype, path, id: idFile } = await storeUpload(
                photo,
            );
            photoUrl = path;
        } else {
            photoUrl = photo;
        }
        const { type, id: idMongo } = fromGlobalId(id);
        const doc = await Shop.findByIdAndUpdate(
            idMongo,
            {
                name,
                desc,
                photo: {
                    url: photoUrl,
                },
            },
            {
                new: true,
            },
        );
        return doc;
    },
});

export const removeShopMutation = mutationWithClientMutationId({
    name: 'removeShop',
    inputFields: {
        id: {
            type: GraphQLID,
        },
        userId: {
            type: GraphQLString,
        },
    },
    outputFields: {
        shop: {
            type: shopType,
            resolve: (payload) => {
                return payload;
            },
        },
    },
    mutateAndGetPayload: async (
        { userId, id }: IShop,
        ctx: PassportContext<User, NextApiRequest>,
    ) => {
        if (userId === ctx.getUser().id) {
            const { type, id: idMongo } = fromGlobalId(id);
            return Shop.findByIdAndRemove(idMongo);
        }
        return {};
    },
});
