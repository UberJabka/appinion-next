import {
    GraphQLID,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLString,
} from 'graphql';
import { globalIdField, mutationWithClientMutationId } from 'graphql-relay';
import { GraphQLUpload } from 'apollo-server-core';
import { FileUpload } from 'graphql-upload';
import { PassportContext } from 'graphql-passport';
import { NextApiRequest } from 'next';
import { AuthenticationError } from 'apollo-server-errors';
import { useRouter } from 'next/router';
import User, { IUser } from '../../models/User';
import ImageType from '../schema/types/ImageType';
import storeUpload from '../../utils/storeUpload';

export const userType = new GraphQLObjectType({
    name: 'User',
    description: 'A single user object',
    fields: {
        id: globalIdField('User', (user) => user._id),
        firstName: { type: GraphQLNonNull(GraphQLString) },
        lastName: { type: GraphQLNonNull(GraphQLString) },
        email: { type: GraphQLNonNull(GraphQLString) },
        phone: { type: GraphQLString },
        photo: { type: ImageType },
        country: { type: GraphQLString },
        city: { type: GraphQLString },
    },
});

export const updateUserMutation = mutationWithClientMutationId({
    name: 'updateUser',
    inputFields: {
        firstName: { type: GraphQLNonNull(GraphQLString) },
        lastName: { type: GraphQLNonNull(GraphQLString) },
        phone: { type: GraphQLString },
        email: { type: GraphQLNonNull(GraphQLString) },
        country: { type: GraphQLString },
        city: { type: GraphQLString },
    },
    outputFields: {
        user: {
            type: userType,
            resolve: (payload) => {
                return payload;
            },
        },
    },
    mutateAndGetPayload: async (
        { firstName, lastName, email, phone, city, country },
        ctx: PassportContext<IUser, NextApiRequest>,
    ) => {
        const userID = await ctx.getUser()._id;
        const doc = await User.findByIdAndUpdate(
            userID,
            {
                firstName,
                lastName,
                email,
                phone,
                city,
                country,
            },
            { new: true },
        );
        return doc;
    },
});

export const updateUserImageMutation = mutationWithClientMutationId({
    name: 'updateUserImage',
    inputFields: {
        photo: { type: GraphQLUpload },
    },
    outputFields: {
        user: {
            type: userType,
            resolve: (payload) => {
                return payload;
            },
        },
    },
    mutateAndGetPayload: async (
        { photo }: { photo: FileUpload },
        ctx: PassportContext<IUser, NextApiRequest>,
    ) => {
        const { filename, mimetype, path, id } = await storeUpload(photo);
        const userID = await ctx.getUser()._id;
        const image = await User.findByIdAndUpdate(
            userID,
            {
                photo: {
                    url: path,
                },
            },
            { new: true },
        );
        return image;
    },
});
export const userLogout = mutationWithClientMutationId({
    name: 'logout',
    inputFields: {},
    outputFields: {
        user: {
            type: userType,
            resolve: (payload) => {
                return payload;
            },
        },
    },
    mutateAndGetPayload: async (
        args,
        ctx: PassportContext<IUser, NextApiRequest>,
    ) => {
        const doc = ctx.getUser();
        ctx.logout();
        return doc;
    },
});
export const loginUserMutation = mutationWithClientMutationId({
    name: 'login',
    inputFields: {
        email: { type: GraphQLNonNull(GraphQLString) },
        password: { type: GraphQLNonNull(GraphQLString) },
    },
    outputFields: {
        user: {
            type: userType,
            resolve: (payload) => {
                return payload;
            },
        },
    },
    mutateAndGetPayload: async ({ email, password }, ctx) => {
        const { user, info } = await ctx.authenticate('graphql-local', {
            email,
            password,
        });
        const doc = await ctx.login(user);
        const t = await ctx.getUser();
        return t;
    },
});
