import {
    GraphQLID,
    GraphQLList,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
} from 'graphql';
import { PassportContext } from 'graphql-passport';
import { NextApiRequest } from 'next';
import { AuthenticationError } from 'apollo-server-errors';
import { fromGlobalId } from 'graphql-relay';
import { IUser } from '../../models/User';
import {
    addShopMutation,
    removeShopMutation,
    shopType,
    updateShopMutation,
} from './shop';
import {
    userType,
    updateUserMutation,
    updateUserImageMutation,
    userLogout,
    loginUserMutation,
} from './user';
import Shop from '../../models/Shop';
import { reviewType } from '../schema';
import Review from '../../models/Review';

const queryType = new GraphQLObjectType({
    name: 'Query',
    fields: {
        me: {
            type: userType,
            args: { id: { type: GraphQLID } },
            resolve(parent, args, ctx: PassportContext<IUser, NextApiRequest>) {
                if (ctx.isUnauthenticated())
                    throw new AuthenticationError('Not authorized');
                return ctx.getUser();
            },
        },
        shops: {
            type: GraphQLList(shopType),
            resolve: (
                parent,
                args,
                ctx: PassportContext<IUser, NextApiRequest>,
            ) => {
                const userID = ctx.getUser()._id;
                const doc = Shop.find({
                    userId: userID,
                });
                return doc;
            },
        },
        shop: {
            type: shopType,
            args: { id: { type: GraphQLID } },
            resolve: (parent, { id }, ctx) => {
                const { type, id: idMongo } = fromGlobalId(id);
                return Shop.findById(idMongo);
            },
        },
        reviews: {
            type: GraphQLList(reviewType),
            args: { shopId: { type: GraphQLString } },
            resolve: (parent, { shopId }) => {
                const doc = Review.find({
                    shopId,
                });
                return doc;
            },
        },
        review: {
            type: reviewType,
            args: { id: { type: GraphQLString } },
            resolve: (parent, { id }) => {
                const doc = Review.find({
                    id,
                });
                return doc;
            },
        },
    },
});

const mutationType = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        updateUser: updateUserMutation,
        updateUserImage: updateUserImageMutation,
        login: loginUserMutation,
        logout: userLogout,
        addShop: addShopMutation,
        updateShop: updateShopMutation,
        removeShop: removeShopMutation,
    },
});

const Schema = new GraphQLSchema({
    query: queryType,
    mutation: mutationType,
});

export default Schema;
