import {
    GraphQLEnumType,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
} from 'graphql';
import {
    connectionArgs,
    connectionDefinitions,
    fromGlobalId,
    globalIdField,
    nodeDefinitions,
    ConnectionArguments,
    mutationWithClientMutationId,
} from 'graphql-relay';
import faker from 'faker';
import { GraphQLUpload } from 'apollo-server-core';
import connectionFromMongooseQuery from '../../utils/connectionFromMongooseQuery';
import Review, { IReview } from '../../models/Review';
import Reply from '../../models/Reply';
import AuthorType from './types/AuthorType';

faker.locale = 'ru';

const { nodeInterface, nodeField } = nodeDefinitions(
    // eslint-disable-next-line consistent-return
    (globalId) => {
        const { type, id } = fromGlobalId(globalId);
        if (type === 'Review') {
            return Review.findById(id).exec();
        }
        if (type === 'Reply') {
            return Reply.findById(id).exec();
        }
    },
    // eslint-disable-next-line no-use-before-define
    (obj) => (obj.rating ? reviewType : replyType),
);

export const reviewType = new GraphQLObjectType({
    name: 'Review',
    description: 'A single review object',
    fields: {
        id: globalIdField('Review', (review) => review._id),
        value: { type: GraphQLString, description: 'The main review text' },
        pros: { type: GraphQLString },
        cons: { type: GraphQLString },
        author: { type: GraphQLNonNull(AuthorType) },
        rating: { type: GraphQLNonNull(GraphQLInt) },
        date: {
            type: GraphQLString,
            resolve: (source) => source.updatedAt,
        },
    },
    interfaces: [nodeInterface],
});

const { connectionType: ReviewConnection } = connectionDefinitions({
    nodeType: reviewType,
});

const replyType = new GraphQLObjectType({
    name: 'Reply',
    fields: {
        reviewId: { type: GraphQLID },
        author: { type: AuthorType },
        value: { type: GraphQLString },
    },
});

const { connectionType: ReplyConnection } = connectionDefinitions({
    nodeType: replyType,
});

const queryType = new GraphQLObjectType({
    name: 'Query',
    fields: {
        reviews: {
            type: GraphQLNonNull(ReviewConnection),
            args: connectionArgs,
            resolve: (parent, args: ConnectionArguments) => {
                return connectionFromMongooseQuery(Review.find(), args);
            },
        },
        replies: {
            type: GraphQLNonNull(ReplyConnection),
            args: connectionArgs,
            resolve: (parent, args: ConnectionArguments) => {
                return connectionFromMongooseQuery(Reply.find(), args);
            },
        },
        node: nodeField,
    },
});

const addReviewMutation = mutationWithClientMutationId({
    name: 'AddReview',
    inputFields: {
        authorName: {
            type: GraphQLNonNull(GraphQLString),
        },
        authorPhoto: {
            type: GraphQLUpload,
        },
        authorLocation: {
            type: GraphQLString,
        },
        authorEmail: {
            type: GraphQLNonNull(GraphQLString),
        },
        value: {
            type: GraphQLString,
        },
        rating: {
            type: GraphQLNonNull(GraphQLInt),
        },
        pros: {
            type: GraphQLString,
        },
        cons: {
            type: GraphQLString,
        },
    },
    outputFields: {
        review: {
            type: reviewType,
        },
    },
    mutateAndGetPayload: async (review): Promise<IReview> => {
        const doc = await Review.create({
            author: {
                name: review.authorName,
                photo: {
                    url: review.authorPhoto,
                },
            },
            value: review.value,
            pros: review.pros,
            rating: review.rating,
            cons: review.cons,
        });
        return doc;
    },
});

const addReviewsSeedMutation = mutationWithClientMutationId({
    name: 'AddReviewSeed',
    inputFields: {
        count: { type: GraphQLNonNull(GraphQLInt) },
    },
    outputFields: {
        message: {
            type: GraphQLString,
        },
    },
    mutateAndGetPayload: async ({ count }) => {
        const seed = new Array(count).fill(0).map(() => {
            return {
                author: {
                    name: faker.name.findName(),
                    location: faker.fake(
                        '{{address.country}}, {{address.city}}',
                    ),
                    photo: {
                        url: faker.internet.avatar(),
                    },
                },
                value: faker.lorem.paragraphs(
                    Math.round((Math.random() * 10) / 2),
                ),
                pros: faker.lorem.sentences(
                    Math.round((Math.random() * 10) / 3),
                ),
                cons: faker.lorem.sentences(
                    Math.round((Math.random() * 10) / 3),
                ),
                public: false,
                shopId: '5f522eefa5cea14aa4186688',
                rating: faker.random.arrayElement([1, 2, 3, 4, 5]),
            };
        });
        await Review.create(seed);
        return {
            message: 'success',
        };
    },
});

const addReplyMutation = mutationWithClientMutationId({
    name: 'AddReply',
    inputFields: {
        reviewId: {
            type: GraphQLNonNull(GraphQLID),
        },
        authorName: {
            type: GraphQLNonNull(GraphQLString),
        },
        authorPhoto: {
            type: GraphQLString,
        },
        location: {
            type: GraphQLString,
        },
        value: {
            type: GraphQLString,
        },
    },
    outputFields: {
        message: { type: GraphQLString },
    },
    mutateAndGetPayload: async (replyData) => {
        const { id } = fromGlobalId(replyData.reviewId);
        await Reply.create({
            reviewId: id,
            author: {
                name: replyData.authorName,
                location: replyData.location,
                photo: {
                    url: replyData.authorPhoto,
                },
            },
            value: replyData.value,
        });
        return {
            message: 'success',
        };
    },
});

const ReactionsEnum = new GraphQLEnumType({
    name: 'Reactions',
    values: {
        LIKE: { value: 'LIKE' },
        DISLIKE: { value: 'DISLIKE' },
    },
});

const addReactionMutation = mutationWithClientMutationId({
    name: 'AddReaction',
    inputFields: {
        reviewId: { type: GraphQLNonNull(GraphQLID) },
        reaction: { type: GraphQLNonNull(ReactionsEnum) },
    },
    outputFields: {
        message: { type: GraphQLString },
    },
    mutateAndGetPayload: async (object) => {
        const { id } = fromGlobalId(object.reviewId);
        const review = await Review.findById(id);
        const index = review.reactions.findIndex(
            (value) => value?.label === object.reaction,
        );
        if (index >= 0) {
            review.reactions[index].count += 1;
        } else {
            review.reactions.push({ label: object.reaction, count: 1 });
        }
        await review.save();
        return {
            message: 'success',
        };
    },
});

const mutationType = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addReview: addReviewMutation,
        addReviewsSeed: addReviewsSeedMutation,
        addReply: addReplyMutation,
        addReaction: addReactionMutation,
    },
});

const Schema = new GraphQLSchema({
    query: queryType,
    mutation: mutationType,
});

export default Schema;
