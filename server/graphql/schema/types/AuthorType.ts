import { GraphQLNonNull, GraphQLObjectType, GraphQLString } from 'graphql';
import ImageType from './ImageType';

const AuthorType = new GraphQLObjectType({
    name: 'Author',
    fields: {
        name: { type: GraphQLNonNull(GraphQLString) },
        photo: { type: ImageType },
        location: { type: GraphQLString },
    },
});

export default AuthorType;
