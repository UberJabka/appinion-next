import { GraphQLNonNull, GraphQLObjectType, GraphQLString } from 'graphql';

const ImageType = new GraphQLObjectType({
    name: 'Image',
    fields: {
        url: { type: GraphQLNonNull(GraphQLString) },
    },
});

export default ImageType;
