import { useEffect, useRef } from 'react';

const useWindowScroll = (callback: (number) => void): void => {
    const lastScrollYPosRef = useRef(-1);
    const isTickingRef = useRef(false);

    const update = () => {
        isTickingRef.current = false;
        callback(lastScrollYPosRef.current);
    };

    const requestTick = () => {
        if (!isTickingRef.current) {
            requestAnimationFrame(update);
        }
        isTickingRef.current = true;
    };

    useEffect(() => {
        const scrollListener = () => {
            lastScrollYPosRef.current = window.scrollY;
            requestTick();
        };

        window.addEventListener('scroll', scrollListener);
        return () => window.removeEventListener('scroll', scrollListener);
    }, []);
};

export default useWindowScroll;
