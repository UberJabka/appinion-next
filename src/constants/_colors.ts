const COLORS = {
    orange: '#FF9800',
    black: '#131018',
    white: '#FFFFFF',
};

export default COLORS;
