import React, { createContext, useContext } from 'react';
import { ApolloError } from '@apollo/client';
import { useGetMeQuery, User } from '../graphql/generated/graphql';

type ContextProps = {
    isAuthenticated: boolean;
    loading: boolean;
    user: User;
    error: ApolloError;
};

const AuthContext = createContext<Partial<ContextProps>>({});

export const AuthProvider = ({ children }) => {
    const { data, loading, error } = useGetMeQuery();

    return (
        <AuthContext.Provider
            value={{
                isAuthenticated: !!data,
                loading,
                user: data?.me,
                error,
            }}>
            {children}
        </AuthContext.Provider>
    );
};

export default function useAuth() {
    const context = useContext(AuthContext);

    return context;
}
