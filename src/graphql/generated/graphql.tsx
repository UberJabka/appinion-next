/* eslint-disable */
import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/client';
import * as ApolloReactHooks from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };

/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
};

export type Query = {
  __typename?: 'Query';
  me?: Maybe<User>;
  shops?: Maybe<Array<Maybe<Shop>>>;
  shop?: Maybe<Shop>;
  reviews?: Maybe<Array<Maybe<Review>>>;
  review?: Maybe<Review>;
};


export type QueryMeArgs = {
  id?: Maybe<Scalars['ID']>;
};


export type QueryShopArgs = {
  id?: Maybe<Scalars['ID']>;
};


export type QueryReviewsArgs = {
  shopId?: Maybe<Scalars['String']>;
};


export type QueryReviewArgs = {
  id?: Maybe<Scalars['String']>;
};

/** A single user object */
export type User = {
  __typename?: 'User';
  /** The ID of an object */
  id: Scalars['ID'];
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  email: Scalars['String'];
  phone?: Maybe<Scalars['String']>;
  photo?: Maybe<Image>;
  country?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
};

export type Image = {
  __typename?: 'Image';
  url: Scalars['String'];
};

/** A single user object */
export type Shop = {
  __typename?: 'Shop';
  /** The ID of an object */
  id: Scalars['ID'];
  name: Scalars['String'];
  desc?: Maybe<Scalars['String']>;
  photo?: Maybe<Image>;
  userId?: Maybe<Scalars['String']>;
};

/** A single review object */
export type Review = Node & {
  __typename?: 'Review';
  /** The ID of an object */
  id: Scalars['ID'];
  /** The main review text */
  value?: Maybe<Scalars['String']>;
  pros?: Maybe<Scalars['String']>;
  cons?: Maybe<Scalars['String']>;
  author: Author;
  rating: Scalars['Int'];
  date?: Maybe<Scalars['String']>;
};

/** An object with an ID */
export type Node = {
  /** The id of the object. */
  id: Scalars['ID'];
};

export type Author = {
  __typename?: 'Author';
  name: Scalars['String'];
  photo?: Maybe<Image>;
  location?: Maybe<Scalars['String']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  updateUser?: Maybe<UpdateUserPayload>;
  updateUserImage?: Maybe<UpdateUserImagePayload>;
  login?: Maybe<LoginPayload>;
  logout?: Maybe<LogoutPayload>;
  addShop?: Maybe<AddShopPayload>;
  updateShop?: Maybe<UpdateShopPayload>;
  removeShop?: Maybe<RemoveShopPayload>;
};


export type MutationUpdateUserArgs = {
  input: UpdateUserInput;
};


export type MutationUpdateUserImageArgs = {
  input: UpdateUserImageInput;
};


export type MutationLoginArgs = {
  input: LoginInput;
};


export type MutationLogoutArgs = {
  input: LogoutInput;
};


export type MutationAddShopArgs = {
  input: AddShopInput;
};


export type MutationUpdateShopArgs = {
  input: UpdateShopInput;
};


export type MutationRemoveShopArgs = {
  input: RemoveShopInput;
};

export type UpdateUserPayload = {
  __typename?: 'updateUserPayload';
  user?: Maybe<User>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type UpdateUserInput = {
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  phone?: Maybe<Scalars['String']>;
  email: Scalars['String'];
  country?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type UpdateUserImagePayload = {
  __typename?: 'updateUserImagePayload';
  user?: Maybe<User>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type UpdateUserImageInput = {
  photo?: Maybe<Scalars['Upload']>;
  clientMutationId?: Maybe<Scalars['String']>;
};


export type LoginPayload = {
  __typename?: 'loginPayload';
  user?: Maybe<User>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type LoginInput = {
  email: Scalars['String'];
  password: Scalars['String'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type LogoutPayload = {
  __typename?: 'logoutPayload';
  user?: Maybe<User>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type LogoutInput = {
  clientMutationId?: Maybe<Scalars['String']>;
};

export type AddShopPayload = {
  __typename?: 'AddShopPayload';
  shop?: Maybe<Shop>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type AddShopInput = {
  userId?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  desc?: Maybe<Scalars['String']>;
  photo?: Maybe<Scalars['Upload']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type UpdateShopPayload = {
  __typename?: 'updateShopPayload';
  shop?: Maybe<Shop>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type UpdateShopInput = {
  id?: Maybe<Scalars['ID']>;
  name: Scalars['String'];
  desc?: Maybe<Scalars['String']>;
  photo?: Maybe<Scalars['Upload']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type RemoveShopPayload = {
  __typename?: 'removeShopPayload';
  shop?: Maybe<Shop>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type RemoveShopInput = {
  id?: Maybe<Scalars['ID']>;
  userId?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type AddShopMutationVariables = Exact<{
  input: AddShopInput;
}>;


export type AddShopMutation = (
  { __typename?: 'Mutation' }
  & { addShop?: Maybe<(
    { __typename?: 'AddShopPayload' }
    & { shop?: Maybe<(
      { __typename?: 'Shop' }
      & Pick<Shop, 'id' | 'name' | 'desc'>
      & { photo?: Maybe<(
        { __typename?: 'Image' }
        & Pick<Image, 'url'>
      )> }
    )> }
  )> }
);

export type LoginMutationVariables = Exact<{
  input: LoginInput;
}>;


export type LoginMutation = (
  { __typename?: 'Mutation' }
  & { login?: Maybe<(
    { __typename?: 'loginPayload' }
    & Pick<LoginPayload, 'clientMutationId'>
  )> }
);

export type LogoutMutationVariables = Exact<{
  input: LogoutInput;
}>;


export type LogoutMutation = (
  { __typename?: 'Mutation' }
  & { logout?: Maybe<(
    { __typename?: 'logoutPayload' }
    & Pick<LogoutPayload, 'clientMutationId'>
  )> }
);

export type RemoveShopMutationVariables = Exact<{
  input: RemoveShopInput;
}>;


export type RemoveShopMutation = (
  { __typename?: 'Mutation' }
  & { removeShop?: Maybe<(
    { __typename?: 'removeShopPayload' }
    & { shop?: Maybe<(
      { __typename?: 'Shop' }
      & Pick<Shop, 'id' | 'name'>
    )> }
  )> }
);

export type UpdateShopMutationVariables = Exact<{
  input: UpdateShopInput;
}>;


export type UpdateShopMutation = (
  { __typename?: 'Mutation' }
  & { updateShop?: Maybe<(
    { __typename?: 'updateShopPayload' }
    & { shop?: Maybe<(
      { __typename?: 'Shop' }
      & Pick<Shop, 'id' | 'name' | 'desc'>
    )> }
  )> }
);

export type UpdateUserMutationVariables = Exact<{
  input: UpdateUserInput;
}>;


export type UpdateUserMutation = (
  { __typename?: 'Mutation' }
  & { updateUser?: Maybe<(
    { __typename?: 'updateUserPayload' }
    & { user?: Maybe<(
      { __typename?: 'User' }
      & Pick<User, 'id' | 'firstName' | 'lastName' | 'email' | 'phone' | 'city' | 'country'>
    )> }
  )> }
);

export type UpdateUserImageMutationVariables = Exact<{
  input: UpdateUserImageInput;
}>;


export type UpdateUserImageMutation = (
  { __typename?: 'Mutation' }
  & { updateUserImage?: Maybe<(
    { __typename?: 'updateUserImagePayload' }
    & { user?: Maybe<(
      { __typename?: 'User' }
      & Pick<User, 'id'>
      & { photo?: Maybe<(
        { __typename?: 'Image' }
        & Pick<Image, 'url'>
      )> }
    )> }
  )> }
);

export type GetMeQueryVariables = Exact<{ [key: string]: never; }>;


export type GetMeQuery = (
  { __typename?: 'Query' }
  & { me?: Maybe<(
    { __typename?: 'User' }
    & Pick<User, 'id' | 'firstName' | 'lastName' | 'email' | 'phone' | 'country' | 'city'>
    & { photo?: Maybe<(
      { __typename?: 'Image' }
      & Pick<Image, 'url'>
    )> }
  )> }
);

export type GetReviewQueryVariables = Exact<{
  id?: Maybe<Scalars['String']>;
}>;


export type GetReviewQuery = (
  { __typename?: 'Query' }
  & { review?: Maybe<(
    { __typename?: 'Review' }
    & Pick<Review, 'id' | 'value' | 'pros' | 'cons' | 'date' | 'rating'>
    & { author: (
      { __typename?: 'Author' }
      & Pick<Author, 'name'>
      & { photo?: Maybe<(
        { __typename?: 'Image' }
        & Pick<Image, 'url'>
      )> }
    ) }
  )> }
);

export type GetReviewsQueryVariables = Exact<{
  shopId?: Maybe<Scalars['String']>;
}>;


export type GetReviewsQuery = (
  { __typename?: 'Query' }
  & { reviews?: Maybe<Array<Maybe<(
    { __typename?: 'Review' }
    & Pick<Review, 'id' | 'value' | 'pros' | 'cons' | 'date'>
    & { author: (
      { __typename?: 'Author' }
      & Pick<Author, 'name'>
      & { photo?: Maybe<(
        { __typename?: 'Image' }
        & Pick<Image, 'url'>
      )> }
    ) }
  )>>> }
);

export type GetShopQueryVariables = Exact<{
  id?: Maybe<Scalars['ID']>;
}>;


export type GetShopQuery = (
  { __typename?: 'Query' }
  & { shop?: Maybe<(
    { __typename?: 'Shop' }
    & Pick<Shop, 'id' | 'name' | 'desc'>
    & { photo?: Maybe<(
      { __typename?: 'Image' }
      & Pick<Image, 'url'>
    )> }
  )> }
);

export type GetShopsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetShopsQuery = (
  { __typename?: 'Query' }
  & { shops?: Maybe<Array<Maybe<(
    { __typename?: 'Shop' }
    & Pick<Shop, 'id' | 'name' | 'desc'>
    & { photo?: Maybe<(
      { __typename?: 'Image' }
      & Pick<Image, 'url'>
    )> }
  )>>> }
);


export const AddShopDocument = gql`
    mutation addShop($input: AddShopInput!) {
  addShop(input: $input) {
    shop {
      id
      name
      desc
      photo {
        url
      }
    }
  }
}
    `;
export type AddShopMutationFn = ApolloReactCommon.MutationFunction<AddShopMutation, AddShopMutationVariables>;

/**
 * __useAddShopMutation__
 *
 * To run a mutation, you first call `useAddShopMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddShopMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addShopMutation, { data, loading, error }] = useAddShopMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useAddShopMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<AddShopMutation, AddShopMutationVariables>) {
        return ApolloReactHooks.useMutation<AddShopMutation, AddShopMutationVariables>(AddShopDocument, baseOptions);
      }
export type AddShopMutationHookResult = ReturnType<typeof useAddShopMutation>;
export type AddShopMutationResult = ApolloReactCommon.MutationResult<AddShopMutation>;
export type AddShopMutationOptions = ApolloReactCommon.BaseMutationOptions<AddShopMutation, AddShopMutationVariables>;
export const LoginDocument = gql`
    mutation login($input: loginInput!) {
  login(input: $input) {
    clientMutationId
  }
}
    `;
export type LoginMutationFn = ApolloReactCommon.MutationFunction<LoginMutation, LoginMutationVariables>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useLoginMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<LoginMutation, LoginMutationVariables>) {
        return ApolloReactHooks.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, baseOptions);
      }
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = ApolloReactCommon.MutationResult<LoginMutation>;
export type LoginMutationOptions = ApolloReactCommon.BaseMutationOptions<LoginMutation, LoginMutationVariables>;
export const LogoutDocument = gql`
    mutation logout($input: logoutInput!) {
  logout(input: $input) {
    clientMutationId
  }
}
    `;
export type LogoutMutationFn = ApolloReactCommon.MutationFunction<LogoutMutation, LogoutMutationVariables>;

/**
 * __useLogoutMutation__
 *
 * To run a mutation, you first call `useLogoutMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLogoutMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [logoutMutation, { data, loading, error }] = useLogoutMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useLogoutMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<LogoutMutation, LogoutMutationVariables>) {
        return ApolloReactHooks.useMutation<LogoutMutation, LogoutMutationVariables>(LogoutDocument, baseOptions);
      }
export type LogoutMutationHookResult = ReturnType<typeof useLogoutMutation>;
export type LogoutMutationResult = ApolloReactCommon.MutationResult<LogoutMutation>;
export type LogoutMutationOptions = ApolloReactCommon.BaseMutationOptions<LogoutMutation, LogoutMutationVariables>;
export const RemoveShopDocument = gql`
    mutation removeShop($input: removeShopInput!) {
  removeShop(input: $input) {
    shop {
      id
      name
    }
  }
}
    `;
export type RemoveShopMutationFn = ApolloReactCommon.MutationFunction<RemoveShopMutation, RemoveShopMutationVariables>;

/**
 * __useRemoveShopMutation__
 *
 * To run a mutation, you first call `useRemoveShopMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRemoveShopMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [removeShopMutation, { data, loading, error }] = useRemoveShopMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useRemoveShopMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<RemoveShopMutation, RemoveShopMutationVariables>) {
        return ApolloReactHooks.useMutation<RemoveShopMutation, RemoveShopMutationVariables>(RemoveShopDocument, baseOptions);
      }
export type RemoveShopMutationHookResult = ReturnType<typeof useRemoveShopMutation>;
export type RemoveShopMutationResult = ApolloReactCommon.MutationResult<RemoveShopMutation>;
export type RemoveShopMutationOptions = ApolloReactCommon.BaseMutationOptions<RemoveShopMutation, RemoveShopMutationVariables>;
export const UpdateShopDocument = gql`
    mutation updateShop($input: updateShopInput!) {
  updateShop(input: $input) {
    shop {
      id
      name
      desc
    }
  }
}
    `;
export type UpdateShopMutationFn = ApolloReactCommon.MutationFunction<UpdateShopMutation, UpdateShopMutationVariables>;

/**
 * __useUpdateShopMutation__
 *
 * To run a mutation, you first call `useUpdateShopMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateShopMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateShopMutation, { data, loading, error }] = useUpdateShopMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateShopMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateShopMutation, UpdateShopMutationVariables>) {
        return ApolloReactHooks.useMutation<UpdateShopMutation, UpdateShopMutationVariables>(UpdateShopDocument, baseOptions);
      }
export type UpdateShopMutationHookResult = ReturnType<typeof useUpdateShopMutation>;
export type UpdateShopMutationResult = ApolloReactCommon.MutationResult<UpdateShopMutation>;
export type UpdateShopMutationOptions = ApolloReactCommon.BaseMutationOptions<UpdateShopMutation, UpdateShopMutationVariables>;
export const UpdateUserDocument = gql`
    mutation updateUser($input: updateUserInput!) {
  updateUser(input: $input) {
    user {
      id
      firstName
      lastName
      email
      phone
      city
      country
    }
  }
}
    `;
export type UpdateUserMutationFn = ApolloReactCommon.MutationFunction<UpdateUserMutation, UpdateUserMutationVariables>;

/**
 * __useUpdateUserMutation__
 *
 * To run a mutation, you first call `useUpdateUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUserMutation, { data, loading, error }] = useUpdateUserMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateUserMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateUserMutation, UpdateUserMutationVariables>) {
        return ApolloReactHooks.useMutation<UpdateUserMutation, UpdateUserMutationVariables>(UpdateUserDocument, baseOptions);
      }
export type UpdateUserMutationHookResult = ReturnType<typeof useUpdateUserMutation>;
export type UpdateUserMutationResult = ApolloReactCommon.MutationResult<UpdateUserMutation>;
export type UpdateUserMutationOptions = ApolloReactCommon.BaseMutationOptions<UpdateUserMutation, UpdateUserMutationVariables>;
export const UpdateUserImageDocument = gql`
    mutation updateUserImage($input: updateUserImageInput!) {
  updateUserImage(input: $input) {
    user {
      id
      photo {
        url
      }
    }
  }
}
    `;
export type UpdateUserImageMutationFn = ApolloReactCommon.MutationFunction<UpdateUserImageMutation, UpdateUserImageMutationVariables>;

/**
 * __useUpdateUserImageMutation__
 *
 * To run a mutation, you first call `useUpdateUserImageMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUserImageMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUserImageMutation, { data, loading, error }] = useUpdateUserImageMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateUserImageMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateUserImageMutation, UpdateUserImageMutationVariables>) {
        return ApolloReactHooks.useMutation<UpdateUserImageMutation, UpdateUserImageMutationVariables>(UpdateUserImageDocument, baseOptions);
      }
export type UpdateUserImageMutationHookResult = ReturnType<typeof useUpdateUserImageMutation>;
export type UpdateUserImageMutationResult = ApolloReactCommon.MutationResult<UpdateUserImageMutation>;
export type UpdateUserImageMutationOptions = ApolloReactCommon.BaseMutationOptions<UpdateUserImageMutation, UpdateUserImageMutationVariables>;
export const GetMeDocument = gql`
    query getMe {
  me {
    id
    firstName
    lastName
    email
    phone
    photo {
      url
    }
    country
    city
  }
}
    `;

/**
 * __useGetMeQuery__
 *
 * To run a query within a React component, call `useGetMeQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetMeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetMeQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetMeQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetMeQuery, GetMeQueryVariables>) {
        return ApolloReactHooks.useQuery<GetMeQuery, GetMeQueryVariables>(GetMeDocument, baseOptions);
      }
export function useGetMeLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetMeQuery, GetMeQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetMeQuery, GetMeQueryVariables>(GetMeDocument, baseOptions);
        }
export type GetMeQueryHookResult = ReturnType<typeof useGetMeQuery>;
export type GetMeLazyQueryHookResult = ReturnType<typeof useGetMeLazyQuery>;
export type GetMeQueryResult = ApolloReactCommon.QueryResult<GetMeQuery, GetMeQueryVariables>;
export const GetReviewDocument = gql`
    query getReview($id: String) {
  review(id: $id) {
    id
    author {
      name
      photo {
        url
      }
    }
    value
    pros
    cons
    date
    rating
  }
}
    `;

/**
 * __useGetReviewQuery__
 *
 * To run a query within a React component, call `useGetReviewQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetReviewQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetReviewQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetReviewQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetReviewQuery, GetReviewQueryVariables>) {
        return ApolloReactHooks.useQuery<GetReviewQuery, GetReviewQueryVariables>(GetReviewDocument, baseOptions);
      }
export function useGetReviewLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetReviewQuery, GetReviewQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetReviewQuery, GetReviewQueryVariables>(GetReviewDocument, baseOptions);
        }
export type GetReviewQueryHookResult = ReturnType<typeof useGetReviewQuery>;
export type GetReviewLazyQueryHookResult = ReturnType<typeof useGetReviewLazyQuery>;
export type GetReviewQueryResult = ApolloReactCommon.QueryResult<GetReviewQuery, GetReviewQueryVariables>;
export const GetReviewsDocument = gql`
    query getReviews($shopId: String) {
  reviews(shopId: $shopId) {
    id
    author {
      name
      photo {
        url
      }
    }
    value
    pros
    cons
    date
  }
}
    `;

/**
 * __useGetReviewsQuery__
 *
 * To run a query within a React component, call `useGetReviewsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetReviewsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetReviewsQuery({
 *   variables: {
 *      shopId: // value for 'shopId'
 *   },
 * });
 */
export function useGetReviewsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetReviewsQuery, GetReviewsQueryVariables>) {
        return ApolloReactHooks.useQuery<GetReviewsQuery, GetReviewsQueryVariables>(GetReviewsDocument, baseOptions);
      }
export function useGetReviewsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetReviewsQuery, GetReviewsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetReviewsQuery, GetReviewsQueryVariables>(GetReviewsDocument, baseOptions);
        }
export type GetReviewsQueryHookResult = ReturnType<typeof useGetReviewsQuery>;
export type GetReviewsLazyQueryHookResult = ReturnType<typeof useGetReviewsLazyQuery>;
export type GetReviewsQueryResult = ApolloReactCommon.QueryResult<GetReviewsQuery, GetReviewsQueryVariables>;
export const GetShopDocument = gql`
    query getShop($id: ID) {
  shop(id: $id) {
    id
    name
    desc
    photo {
      url
    }
  }
}
    `;

/**
 * __useGetShopQuery__
 *
 * To run a query within a React component, call `useGetShopQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetShopQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetShopQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetShopQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetShopQuery, GetShopQueryVariables>) {
        return ApolloReactHooks.useQuery<GetShopQuery, GetShopQueryVariables>(GetShopDocument, baseOptions);
      }
export function useGetShopLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetShopQuery, GetShopQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetShopQuery, GetShopQueryVariables>(GetShopDocument, baseOptions);
        }
export type GetShopQueryHookResult = ReturnType<typeof useGetShopQuery>;
export type GetShopLazyQueryHookResult = ReturnType<typeof useGetShopLazyQuery>;
export type GetShopQueryResult = ApolloReactCommon.QueryResult<GetShopQuery, GetShopQueryVariables>;
export const GetShopsDocument = gql`
    query getShops {
  shops {
    id
    name
    desc
    photo {
      url
    }
  }
}
    `;

/**
 * __useGetShopsQuery__
 *
 * To run a query within a React component, call `useGetShopsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetShopsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetShopsQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetShopsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetShopsQuery, GetShopsQueryVariables>) {
        return ApolloReactHooks.useQuery<GetShopsQuery, GetShopsQueryVariables>(GetShopsDocument, baseOptions);
      }
export function useGetShopsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetShopsQuery, GetShopsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetShopsQuery, GetShopsQueryVariables>(GetShopsDocument, baseOptions);
        }
export type GetShopsQueryHookResult = ReturnType<typeof useGetShopsQuery>;
export type GetShopsLazyQueryHookResult = ReturnType<typeof useGetShopsLazyQuery>;
export type GetShopsQueryResult = ApolloReactCommon.QueryResult<GetShopsQuery, GetShopsQueryVariables>;