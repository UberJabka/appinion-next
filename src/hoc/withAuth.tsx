import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import useAuth from '../containers/AuthProvider';
import Loader from '../components/Layout/Loader';

const withAuth = <P extends Record<string, unknown>>(
    Component: React.ComponentType<P>,
): React.FC<P> => ({ ...props }) => {
    const { isAuthenticated, loading } = useAuth();
    const router = useRouter();
    useEffect(() => {
        if (!isAuthenticated && !loading) router.push('/login');
    }, [loading, isAuthenticated]);
    if (loading && !isAuthenticated) {
        return <Loader />;
    }
    if (isAuthenticated && !loading) {
        return <Component {...(props as P)} />;
    }
    return <></>;
};
export default withAuth;
