import React, { useEffect } from 'react';
import { AppProps } from 'next/app';
import styled, { ThemeProvider } from 'styled-components';
import { useRouter } from 'next/router';
import { MuiThemeProvider, CssBaseline } from '@material-ui/core';
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
import { createUploadLink } from 'apollo-upload-client';
import TopBar from '../components/Layout/TopBar';
import NavBar from '../components/Layout/Navbar';
import theme from '../constants/theme';
import { AuthProvider } from '../containers/AuthProvider';

const Root = styled.div`
    background-color: #f4f6f8;
    display: flex;
    height: 100%;
    overflow: hidden;
    width: 100%;
`;
const Wrapper = styled.div<{ auth: boolean }>`
    display: flex;
    flex: 1 1 auto;
    overflow: hidden;
    padding-top: 64px;
    min-height: 100vh;
    @media (min-width: 1280px) {
        padding-left: ${(auth) => (auth.auth ? '256px' : '0')};
    }
`;
const ContentContainer = styled.div`
    display: flex;
    flex: 1 1 auto;
    overflow: hidden;
`;
const Content = styled.div`
    flex: 1 1 auto;
    height: 100%;
    overflow: auto;
    min-height: 100%;
    padding-top: 24px;
    padding-bottom: 24px;
    background-color: #f4f6f8;
    @media (min-width: 600px) {
        padding-left: 24px;
        padding-right: 24px;
    }
`;

const CustomApp: React.FC<AppProps> = ({ Component, pageProps }) => {
    const router = useRouter();
    useEffect(() => {
        const jssStyles = document.querySelector('#jss-server-side');
        if (jssStyles) {
            jssStyles.parentNode.removeChild(jssStyles);
        }
    }, []);

    const client = new ApolloClient({
        cache: new InMemoryCache(),
        link: createUploadLink({
            uri: 'http://localhost:3000/api/graphql-admin',
        }),
        connectToDevTools: true,
    });

    const auth = (): boolean => {
        if (router.pathname === '/login' || router.pathname === '/register') {
            return false;
        }
        return true;
    };

    if (router.pathname !== '/') {
        return (
            <>
                <MuiThemeProvider theme={theme}>
                    <ThemeProvider theme={theme}>
                        <ApolloProvider client={client}>
                            <AuthProvider>
                                <CssBaseline />
                                <Root>
                                    <TopBar />
                                    {auth() ? <NavBar /> : <></>}
                                    <Wrapper auth={auth()}>
                                        <ContentContainer>
                                            <Content>
                                                <Component {...pageProps} />
                                            </Content>
                                        </ContentContainer>
                                    </Wrapper>
                                </Root>
                            </AuthProvider>
                        </ApolloProvider>
                    </ThemeProvider>
                </MuiThemeProvider>
            </>
        );
    }
    return (
        <>
            <Component {...pageProps} />
        </>
    );
};

export default CustomApp;
