import React from 'react';
import Head from 'next/head';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import {
    Card,
    CardHeader,
    Divider,
    CardContent,
    Grid,
    Typography,
    FormControlLabel,
    Checkbox,
    Button,
    TextField,
} from '@material-ui/core';
import styled from 'styled-components';

const Item = styled(Grid)`
    display: flex;
    flex-direction: column;
`;

const Dashboard: React.FC = () => {
    return (
        <>
            <Head>
                <title>Dashboard</title>
            </Head>
            <Container maxWidth="lg">
                <>
                    <form>
                        <Card>
                            <CardHeader
                                subheader="Manage the notifications"
                                title="Notifications"
                            />
                            <Divider />
                            <CardContent>
                                <Grid container spacing={6} wrap="wrap">
                                    <Item item md={4} sm={6} xs={12}>
                                        <Typography
                                            color="textPrimary"
                                            gutterBottom
                                            variant="h6">
                                            Notifications
                                        </Typography>
                                        <FormControlLabel
                                            control={
                                                <Checkbox defaultChecked />
                                            }
                                            label="Email"
                                        />
                                        <FormControlLabel
                                            control={
                                                <Checkbox defaultChecked />
                                            }
                                            label="Push Notifications"
                                        />
                                        <FormControlLabel
                                            control={<Checkbox />}
                                            label="Text Messages"
                                        />
                                        <FormControlLabel
                                            control={
                                                <Checkbox defaultChecked />
                                            }
                                            label="Phone calls"
                                        />
                                    </Item>
                                    <Item item md={4} sm={6} xs={12}>
                                        <Typography
                                            color="textPrimary"
                                            gutterBottom
                                            variant="h6">
                                            Messages
                                        </Typography>
                                        <FormControlLabel
                                            control={
                                                <Checkbox defaultChecked />
                                            }
                                            label="Email"
                                        />
                                        <FormControlLabel
                                            control={<Checkbox />}
                                            label="Push Notifications"
                                        />
                                        <FormControlLabel
                                            control={
                                                <Checkbox defaultChecked />
                                            }
                                            label="Phone calls"
                                        />
                                    </Item>
                                </Grid>
                            </CardContent>
                            <Divider />
                            <Box display="flex" justifyContent="flex-end" p={2}>
                                <Button color="primary" variant="contained">
                                    Save
                                </Button>
                            </Box>
                        </Card>
                    </form>
                </>
                <Box mt={3}>
                    <>
                        <form>
                            <Card>
                                <CardHeader
                                    subheader="Update password"
                                    title="Password"
                                />
                                <Divider />
                                <CardContent>
                                    <TextField
                                        fullWidth
                                        label="Password"
                                        margin="normal"
                                        name="password"
                                        type="password"
                                        variant="outlined"
                                    />
                                    <TextField
                                        fullWidth
                                        label="Confirm password"
                                        margin="normal"
                                        name="confirm"
                                        type="password"
                                        variant="outlined"
                                    />
                                </CardContent>
                                <Divider />
                                <Box
                                    display="flex"
                                    justifyContent="flex-end"
                                    p={2}>
                                    <Button color="primary" variant="contained">
                                        Update
                                    </Button>
                                </Box>
                            </Card>
                        </form>
                    </>
                </Box>
            </Container>
        </>
    );
};

export default Dashboard;
