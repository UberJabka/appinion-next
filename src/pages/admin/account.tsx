import React from 'react';
import Head from 'next/head';
import {
    Container,
    Grid,
    Card,
    CardContent,
    Box,
    Avatar,
    Typography,
    Divider,
    CardActions,
    Button,
    TextField,
    CardHeader,
} from '@material-ui/core';
import styled from 'styled-components';
import * as Yup from 'yup';
import { useFormik } from 'formik';
import { useRouter } from 'next/router';
import withAuth from '../../hoc/withAuth';
import useAuth from '../../containers/AuthProvider';
import {
    useLogoutMutation,
    useUpdateUserImageMutation,
    useUpdateUserMutation,
} from '../../graphql/generated/graphql';
import AppAlert from '../../components/Layout/AppAlert';

const AppAvatar = styled(Avatar)`
    height: 100px !important;
    width: 100px !important;
`;
const AppCard = styled(Card)`
    ${({ theme }) => `
    background-color: ${theme.palette.background.dark} ;
    padding-bottom: ${theme.spacing(3)};
    padding-top: ${theme.spacing(3)};`}
`;
const InputHidden = styled.input`
    display: none;
`;
const Account: React.FC = () => {
    const { user } = useAuth();
    const [updateUserPhoto, { loading, error }] = useUpdateUserImageMutation();
    const [updateUser, dataUpdateUser] = useUpdateUserMutation();
    const photoForm = useFormik({
        initialValues: {
            photo: null,
        },
        onSubmit: (values) =>
            updateUserPhoto({
                variables: {
                    input: {
                        photo: values.photo,
                        clientMutationId: user.id,
                    },
                },
            }),
        enableReinitialize: true,
    });
    const userForm = useFormik({
        initialValues: {
            firstName: `${user?.firstName}`,
            lastName: `${user?.lastName}`,
            email: `${user?.email}`,
            phone: user?.phone ? `${user?.phone}` : '',
            country: user?.country ? `${user?.country}` : '',
            city: user?.city ? `${user?.city}` : '',
        },
        onSubmit: (values) => {
            updateUser({
                variables: {
                    input: {
                        firstName: values.firstName,
                        lastName: values.lastName,
                        email: values.email,
                        phone: values.phone,
                        country: values.country,
                        city: values.city,
                    },
                },
            });
        },
        enableReinitialize: true,
        validationSchema: Yup.object().shape({
            email: Yup.string()
                .email('Must be a valid email')
                .max(255)
                .required('Email обязателен'),
            firstName: Yup.string().max(255).required('Имя обязательное'),
            lastName: Yup.string().max(255).required('Фамилия обязательная'),
            phone: Yup.string().max(255).required('Телефон обязательный'),
            city: Yup.string().max(255).required('Город обязателен'),
            country: Yup.string().max(255).required('Страна обязательна'),
        }),
    });
    const [logout, { data: logoutData }] = useLogoutMutation();
    if (logoutData) {
        useRouter().push('/');
    }
    return (
        <>
            <Head>
                <title>Настройка пользователя</title>
            </Head>
            <Container maxWidth="lg">
                <Grid container spacing={3}>
                    <Grid item lg={4} md={6} xs={12}>
                        <AppCard>
                            <CardContent>
                                <Box
                                    alignItems="center"
                                    display="flex"
                                    flexDirection="column">
                                    <AppAvatar src={user?.photo?.url} />
                                    {(user?.firstName || user?.lastName) && (
                                        <Typography
                                            color="textPrimary"
                                            gutterBottom
                                            variant="h3">
                                            {`${user?.firstName}  ${user?.lastName}`}
                                        </Typography>
                                    )}
                                    {(user?.city || user?.country) && (
                                        <Typography
                                            color="textSecondary"
                                            variant="body1">
                                            {`${user?.city}, ${user?.country}`}
                                        </Typography>
                                    )}
                                </Box>
                            </CardContent>
                            <Divider />
                            <CardActions>
                                <Button
                                    color="primary"
                                    fullWidth
                                    variant="text">
                                    <label>
                                        <InputHidden
                                            type="file"
                                            onChange={(event) => {
                                                photoForm.setFieldValue(
                                                    'photo',
                                                    event.currentTarget
                                                        .files[0],
                                                );
                                                photoForm.submitForm();
                                            }}
                                        />
                                        Обновить фото
                                    </label>
                                </Button>
                            </CardActions>
                        </AppCard>
                        <Button
                            color="primary"
                            variant="contained"
                            onClick={() => {
                                logout({
                                    variables: {
                                        input: {
                                            clientMutationId: user.id,
                                        },
                                    },
                                });
                            }}>
                            Выйти из профиля
                        </Button>
                    </Grid>
                    <Grid item lg={8} md={6} xs={12}>
                        <form onSubmit={userForm.handleSubmit}>
                            <Card>
                                <CardHeader
                                    subheader="Редактировать информацию"
                                    title="Профиль"
                                />
                                <Divider />
                                <CardContent>
                                    <Grid container spacing={3}>
                                        <Grid item md={6} xs={12}>
                                            <TextField
                                                fullWidth
                                                label="Имя"
                                                name="firstName"
                                                required
                                                onChange={userForm.handleChange}
                                                variant="outlined"
                                                value={
                                                    userForm.values.firstName
                                                }
                                            />
                                        </Grid>
                                        <Grid item md={6} xs={12}>
                                            <TextField
                                                fullWidth
                                                label="Фамилия"
                                                name="lastName"
                                                required
                                                onChange={userForm.handleChange}
                                                variant="outlined"
                                                value={userForm.values.lastName}
                                            />
                                        </Grid>
                                        <Grid item md={6} xs={12}>
                                            <TextField
                                                fullWidth
                                                label="Email"
                                                margin="normal"
                                                name="email"
                                                onBlur={userForm.handleBlur}
                                                onChange={userForm.handleChange}
                                                type="email"
                                                value={userForm.values.email}
                                                variant="outlined"
                                            />
                                        </Grid>
                                        <Grid item md={6} xs={12}>
                                            <TextField
                                                fullWidth
                                                label="Телефон"
                                                name="phone"
                                                onChange={userForm.handleChange}
                                                variant="outlined"
                                                value={userForm.values.phone}
                                            />
                                        </Grid>
                                        <Grid item md={6} xs={12}>
                                            <TextField
                                                fullWidth
                                                label="Страна"
                                                name="country"
                                                onChange={userForm.handleChange}
                                                variant="outlined"
                                                value={userForm.values.country}
                                            />
                                        </Grid>
                                        <Grid item md={6} xs={12}>
                                            <TextField
                                                fullWidth
                                                label="Город"
                                                name="city"
                                                variant="outlined"
                                                onChange={userForm.handleChange}
                                                value={userForm.values.city}
                                            />
                                        </Grid>
                                    </Grid>
                                </CardContent>
                                <Divider />
                                <Box
                                    display="flex"
                                    justifyContent="flex-end"
                                    p={2}>
                                    <Button
                                        type="submit"
                                        disabled={dataUpdateUser.loading}
                                        color="primary"
                                        variant="contained">
                                        Сохранить
                                    </Button>
                                </Box>
                            </Card>
                        </form>
                        {dataUpdateUser.data && (
                            <AppAlert
                                text="Профиль успешно обновлен"
                                type="success"
                            />
                        )}
                        {dataUpdateUser.error && (
                            <AppAlert
                                text="Ошибка обновления профиля"
                                type="error"
                            />
                        )}
                    </Grid>
                </Grid>
            </Container>
        </>
    );
};

export default withAuth(Account);
