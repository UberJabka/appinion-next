import React from 'react';
import Head from 'next/head';
import {
    Box,
    Button,
    Card,
    CardContent,
    CardHeader,
    Container,
    Divider,
    Grid,
    TextField,
} from '@material-ui/core';
import Link from 'next/link';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import withAuth from '../../../hoc/withAuth';
import { useAddShopMutation } from '../../../graphql/generated/graphql';
import AppAlert from '../../../components/Layout/AppAlert';

const Add: React.FC = () => {
    const [addShopMutation, { data, loading, error }] = useAddShopMutation();
    const addForm = useFormik({
        initialValues: {
            name: '',
            desc: '',
            photo: null,
        },
        onSubmit: (values) => {
            addShopMutation({
                variables: {
                    input: {
                        name: values.name,
                        desc: values.desc,
                        photo: values.photo,
                    },
                },
            });
        },
        validationSchema: Yup.object().shape({
            name: Yup.string()
                .max(255)
                .required('Название магазина обязательно'),
            desc: Yup.string().max(255),
        }),
        enableReinitialize: true,
    });

    return (
        <>
            <Head>
                <title>Магазины</title>
            </Head>
            <Container maxWidth={false}>
                <Box mb={3}>
                    <h1>Добавить новый магазин</h1>
                    <Link href="/admin/shops">
                        <Button color="primary" variant="contained">
                            Вернуться к списку магазинов
                        </Button>
                    </Link>
                </Box>
                {data && (
                    <Grid item lg={10} md={6} xs={12}>
                        <AppAlert
                            text="Магазин успешно создан"
                            type="success"
                        />
                    </Grid>
                )}
                {error && (
                    <Grid item lg={10} md={6} xs={12}>
                        <AppAlert
                            text="Ошибка создание магазина"
                            type="error"
                        />
                    </Grid>
                )}
                <form onSubmit={addForm.handleSubmit}>
                    <Grid container spacing={3}>
                        <Grid item lg={10} md={6} xs={12}>
                            <Card>
                                <CardHeader title="Добавить магазин" />
                                <Divider />
                                <CardContent>
                                    <Grid container spacing={3}>
                                        <Grid item xs={12}>
                                            <TextField
                                                label="Иконка магазина"
                                                type="file"
                                                fullWidth
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                name="photo"
                                                onChange={(event) => {
                                                    addForm.setFieldValue(
                                                        'photo',
                                                        event.currentTarget
                                                            .files[0],
                                                    );
                                                }}
                                                variant="outlined"
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField
                                                fullWidth
                                                label="Название магазина"
                                                name="name"
                                                required
                                                onChange={addForm.handleChange}
                                                variant="outlined"
                                                value={addForm.values.name}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField
                                                fullWidth
                                                label="Описание магазина"
                                                name="desc"
                                                onChange={addForm.handleChange}
                                                variant="outlined"
                                                value={addForm.values.desc}
                                                rows={7}
                                            />
                                        </Grid>
                                    </Grid>
                                </CardContent>
                                <Divider />
                                <Box
                                    display="flex"
                                    justifyContent="flex-end"
                                    p={2}>
                                    <Button
                                        type="submit"
                                        color="primary"
                                        variant="contained">
                                        Создать
                                    </Button>
                                </Box>
                            </Card>
                        </Grid>
                    </Grid>
                </form>
            </Container>
        </>
    );
};

export default withAuth(Add);
