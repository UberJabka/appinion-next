import React from 'react';
import Head from 'next/head';
import {
    Avatar,
    Box,
    Button,
    Card,
    CardContent,
    CardHeader,
    Container,
    Divider,
    Grid,
    TextField,
} from '@material-ui/core';
import Link from 'next/link';
import { useRouter } from 'next/router';
import ErrorPage from 'next/error';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import styled from 'styled-components';
import withAuth from '../../../../hoc/withAuth';
import {
    useGetReviewsQuery,
    useGetShopQuery,
    useUpdateShopMutation,
} from '../../../../graphql/generated/graphql';
import ReviewsList from '../../../../components/Layout/ReviewsList';
import Loader from '../../../../components/Layout/Loader';
import AppAlert from '../../../../components/Layout/AppAlert';

const IconShop = styled(Grid)`
    display: flex;
    align-items: center;
    justify-content: flex-end;
`;

const ShopPage: React.FC = () => {
    const router = useRouter();
    const {
        data: queryShopData,
        loading: queryShopLoading,
        error: queryShopError,
    } = useGetShopQuery({
        variables: {
            id: router.query.id,
        },
    });
    const [
        updateShop,
        {
            data: updateShopData,
            loading: updateShopLoading,
            error: updateShopError,
        },
    ] = useUpdateShopMutation();
    const updateForm = useFormik({
        initialValues: {
            name: `${queryShopData?.shop.name}`,
            desc: queryShopData?.shop?.desc
                ? `${queryShopData?.shop?.desc}`
                : '',
        },
        onSubmit: (values) => {
            let photo = null;
            if (values.photo) {
                photo = values.photo;
            } else {
                photo = queryShopData?.shop?.photo.url;
            }
            updateShop({
                variables: {
                    input: {
                        id: queryShopData?.shop.id,
                        clientMutationId: queryShopData?.shop.id,
                        name: values.name,
                        desc: values.desc,
                        photo,
                    },
                },
            });
        },
        validationSchema: Yup.object().shape({
            name: Yup.string()
                .max(255)
                .required('Название магазина обязательно'),
            desc: Yup.string().max(255),
        }),
        enableReinitialize: true,
    });
    if (queryShopError) {
        return <ErrorPage statusCode={404} />;
    }
    const {
        data: dataReviews,
        loading: loadingReview,
        error: errorReview,
    } = useGetReviewsQuery({
        variables: {
            shopId: `${router.query.id}`,
        },
    });

    return (
        <>
            <Head>
                <title>
                    {queryShopData?.shop.name
                        ? queryShopData.shop.name
                        : 'Магазины'}
                </title>
            </Head>
            {!queryShopLoading ? (
                <Container maxWidth={false}>
                    <Box mb={3}>
                        <Grid container spacing={1}>
                            <Grid item xs={7}>
                                {queryShopData?.shop.name && (
                                    <h1>{queryShopData.shop.name}</h1>
                                )}
                                {queryShopData?.shop.desc && (
                                    <Box mb={3}>{queryShopData.shop.desc}</Box>
                                )}
                                <Link href="/admin/shops">
                                    <Button color="primary" variant="contained">
                                        Вернуться к списку магазинов
                                    </Button>
                                </Link>
                            </Grid>
                            <IconShop item xs={3}>
                                {queryShopData?.shop?.photo.url && (
                                    <Avatar
                                        alt="Product"
                                        src={queryShopData?.shop?.photo.url}
                                        variant="square"
                                    />
                                )}
                            </IconShop>
                        </Grid>
                    </Box>
                    <form onSubmit={updateForm.handleSubmit}>
                        <Grid container spacing={3}>
                            <Grid item lg={10} md={6} xs={12}>
                                {updateShopData && (
                                    <AppAlert
                                        text="Магазин обновлен"
                                        type="success"
                                    />
                                )}
                                {updateShopError && (
                                    <AppAlert
                                        text="Ошибка обновления магазина"
                                        type="error"
                                    />
                                )}
                                <Card>
                                    <CardHeader title="Редактировать магазин" />
                                    <Divider />
                                    <CardContent>
                                        <Grid container spacing={3}>
                                            <Grid item xs={7}>
                                                <TextField
                                                    fullWidth
                                                    label="Название магазина"
                                                    name="name"
                                                    required
                                                    onChange={
                                                        updateForm.handleChange
                                                    }
                                                    variant="outlined"
                                                    value={
                                                        updateForm.values.name
                                                    }
                                                />
                                            </Grid>
                                            <Grid item xs={5}>
                                                <TextField
                                                    type="file"
                                                    fullWidth
                                                    label="Иконка магазина"
                                                    name="photo"
                                                    variant="outlined"
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                    onChange={(event) => {
                                                        updateForm.setFieldValue(
                                                            'photo',
                                                            event.currentTarget
                                                                .files[0],
                                                        );
                                                    }}
                                                />
                                            </Grid>
                                            <Grid item xs={12}>
                                                <TextField
                                                    fullWidth
                                                    label="Описание магазина"
                                                    name="desc"
                                                    onChange={
                                                        updateForm.handleChange
                                                    }
                                                    variant="outlined"
                                                    value={
                                                        updateForm.values.desc
                                                    }
                                                    rows={7}
                                                />
                                            </Grid>
                                        </Grid>
                                    </CardContent>
                                    <Divider />
                                    <Box
                                        display="flex"
                                        justifyContent="flex-end"
                                        p={2}>
                                        <Button
                                            type="submit"
                                            color="primary"
                                            variant="contained">
                                            Обновить
                                        </Button>
                                    </Box>
                                </Card>
                            </Grid>
                        </Grid>
                    </form>
                    <Box mt={3}>
                        {dataReviews?.reviews.length !== 0 && (
                            <ReviewsList reviews={dataReviews?.reviews} />
                        )}
                    </Box>
                </Container>
            ) : (
                <Loader />
            )}
        </>
    );
};

export default withAuth(ShopPage);
