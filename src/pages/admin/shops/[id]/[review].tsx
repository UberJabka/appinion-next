import React from 'react';
import Head from 'next/head';
import { Box, Button, Container } from '@material-ui/core';
import Link from 'next/link';
import { useRouter } from 'next/router';
import withAuth from '../../../../hoc/withAuth';

const ShopPage: React.FC = () => {
    const router = useRouter();
    console.log(router.query);

    return (
        <>
            <Head>
                <title>Комментарий</title>
            </Head>
            <Container maxWidth={false}>
                <Box mb={3}>
                    <h1>Внутренняя страница комментария</h1>
                    <Link
                        href="/admin/shops/[id]"
                        as={`/admin/shops/${router.query.id}`}>
                        <Button color="primary" variant="contained">
                            Вернуться в магазин
                        </Button>
                    </Link>
                </Box>
            </Container>
        </>
    );
};

export default withAuth(ShopPage);
