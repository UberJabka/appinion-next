import React from 'react';
import Head from 'next/head';
import { Box, Button, Container, Grid } from '@material-ui/core';
import styled from 'styled-components';
import Link from 'next/link';
import { Alert, AlertTitle } from '@material-ui/lab';
import ShopCard from '../../../components/Layout/ShopCard';
import { useGetShopsQuery } from '../../../graphql/generated/graphql';
import Loader from '../../../components/Layout/Loader';
import AppAlert from '../../../components/Layout/AppAlert';
import withAuth from '../../../hoc/withAuth';

const AppShopCard = styled(ShopCard)`
    height: 100%;
`;
const AppContainer = styled(Container)`
    height: 100%;
`;

const Index: React.FC = () => {
    const { data, loading, error } = useGetShopsQuery();

    return (
        <>
            <Head>
                <title>Магазины</title>
            </Head>
            <AppContainer maxWidth={false} style={{ height: '100%' }}>
                <>
                    <Box display="flex" justifyContent="flex-end">
                        <Link href="/admin/shops/add">
                            <Button color="primary" variant="contained">
                                Добавить магазин
                            </Button>
                        </Link>
                    </Box>
                </>
                {loading ? (
                    <Loader />
                ) : (
                    <Box mt={3}>
                        {error && (
                            <AppAlert
                                text="Ошибка получение магазинов"
                                type="error"
                            />
                        )}
                        {!data?.shops.length && !loading ? (
                            <>
                                <h2>Магазинов не найдено</h2>
                            </>
                        ) : (
                            data && (
                                <Grid container spacing={3}>
                                    {data?.shops.map((shop) => (
                                        <Grid
                                            item
                                            key={shop.id}
                                            lg={4}
                                            md={6}
                                            xs={12}>
                                            <AppShopCard shop={shop} />
                                        </Grid>
                                    ))}
                                </Grid>
                            )
                        )}
                    </Box>
                )}
            </AppContainer>
        </>
    );
};

export default withAuth(Index);
