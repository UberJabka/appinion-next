import React from 'react';
import Head from 'next/head';
import { Button } from '@material-ui/core';
import Link from 'next/link';

const Home: React.FC = () => {
    return (
        <>
            <Head>
                <title>Apinion</title>
            </Head>
            <h1>Главная страница</h1>
            <Link href="/login">
                <Button variant="contained" color="primary">
                    Войти
                </Button>
            </Link>
            <Link href="/register">
                <Button variant="contained">Регистрация</Button>
            </Link>
        </>
    );
};

export default Home;
