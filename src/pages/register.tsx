import React from 'react';
import Head from 'next/head';
import {
    Box,
    Container,
    TextField,
    Typography,
    Checkbox,
    FormHelperText,
    Button,
} from '@material-ui/core';
import { Formik } from 'formik';
import * as Yup from 'yup';
import Link from 'next/link';

const Login: React.FC = () => {
    return (
        <>
            <Head>
                <title>Регистрация</title>
            </Head>
            <Box
                display="flex"
                flexDirection="column"
                height="100%"
                justifyContent="center">
                <Container maxWidth="sm">
                    <Formik
                        initialValues={{
                            email: '',
                            firstName: '',
                            lastName: '',
                            password: '',
                            policy: false,
                        }}
                        validationSchema={Yup.object().shape({
                            email: Yup.string()
                                .email('Email должен быть валидный ')
                                .max(255)
                                .required('Поле Email обязательно'),
                            firstName: Yup.string()
                                .max(255)
                                .required('Поле Ваше имя обязательно'),
                            lastName: Yup.string()
                                .max(255)
                                .required('Поле Фамилия обязательно'),
                            password: Yup.string()
                                .max(255)
                                .required('Поле Пароль обязательно'),
                            policy: Yup.boolean().oneOf(
                                [true],
                                'This field must be checked',
                            ),
                        })}
                        onSubmit={async (values) => {
                            try {
                                const res = await fetch('/api/register', {
                                    method: 'POST',
                                    body: JSON.stringify(values),
                                    headers: {
                                        'Content-Type': 'application/json',
                                    },
                                });
                                console.log('Успешная Регистрация:', res);
                            } catch (e) {
                                console.error('error - ', e);
                            }
                        }}>
                        {({
                            errors,
                            handleBlur,
                            handleChange,
                            handleSubmit,
                            isSubmitting,
                            touched,
                            values,
                        }) => (
                            <form onSubmit={handleSubmit}>
                                <Box mb={3}>
                                    <Typography
                                        color="textPrimary"
                                        variant="h2">
                                        Регистрация
                                    </Typography>
                                    <Typography
                                        color="textSecondary"
                                        gutterBottom
                                        variant="body2">
                                        Используйте Email для создания аккаунта
                                    </Typography>
                                </Box>
                                <TextField
                                    error={Boolean(
                                        touched.firstName && errors.firstName,
                                    )}
                                    fullWidth
                                    helperText={
                                        touched.firstName && errors.firstName
                                    }
                                    label="Ваше имя"
                                    margin="normal"
                                    name="firstName"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    value={values.firstName}
                                    variant="outlined"
                                />
                                <TextField
                                    error={Boolean(
                                        touched.lastName && errors.lastName,
                                    )}
                                    fullWidth
                                    helperText={
                                        touched.lastName && errors.lastName
                                    }
                                    label="Фамилия"
                                    margin="normal"
                                    name="lastName"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    value={values.lastName}
                                    variant="outlined"
                                />
                                <TextField
                                    error={Boolean(
                                        touched.email && errors.email,
                                    )}
                                    fullWidth
                                    helperText={touched.email && errors.email}
                                    label="Email"
                                    margin="normal"
                                    name="email"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    type="email"
                                    value={values.email}
                                    variant="outlined"
                                />
                                <TextField
                                    error={Boolean(
                                        touched.password && errors.password,
                                    )}
                                    fullWidth
                                    helperText={
                                        touched.password && errors.password
                                    }
                                    label="Пароль"
                                    margin="normal"
                                    name="password"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    type="password"
                                    value={values.password}
                                    variant="outlined"
                                />
                                <Box alignItems="center" display="flex" ml={-1}>
                                    <Checkbox
                                        checked={values.policy}
                                        name="policy"
                                        onChange={handleChange}
                                    />
                                    <Typography
                                        color="textSecondary"
                                        variant="body1">
                                        Согласие политики конфединциальности
                                        {/* <Link href="#"> */}
                                        {/*    <a>Terms and Conditions</a> */}
                                        {/* </Link> */}
                                    </Typography>
                                </Box>
                                {Boolean(touched.policy && errors.policy) && (
                                    <FormHelperText error>
                                        {errors.policy}
                                    </FormHelperText>
                                )}
                                <Box my={2}>
                                    <Button
                                        color="primary"
                                        disabled={isSubmitting}
                                        fullWidth
                                        size="large"
                                        type="submit"
                                        variant="contained">
                                        Зарегистрироваться
                                    </Button>
                                </Box>
                                <Typography
                                    color="textSecondary"
                                    variant="body1">
                                    Есть аккаунт?{' '}
                                    <Link href="/admin">
                                        <a>Войти</a>
                                    </Link>
                                </Typography>
                            </form>
                        )}
                    </Formik>
                </Container>
            </Box>
        </>
    );
};

export default Login;
