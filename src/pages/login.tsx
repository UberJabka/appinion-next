import React from 'react';
import Head from 'next/head';
import {
    Box,
    Container,
    Grid,
    Typography,
    Button,
    TextField,
} from '@material-ui/core';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import FacebookIcon from '@material-ui/icons/Facebook';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useLoginMutation } from '../graphql/generated/graphql';

const Login: React.FC = () => {
    const [login, { data, loading, error }] = useLoginMutation();
    const loginForm = useFormik({
        initialValues: {
            email: 'test@mail.ru',
            password: '123',
        },
        onSubmit: async (values) => {
            try {
                const res = await fetch('/api/auth/', {
                    method: 'POST',
                    body: JSON.stringify(values),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
                console.log('Успешная авторизация:', res);
            } catch (e) {
                console.error('error - ', e);
            }
            // await login({
            //     variables: {
            //         input: {
            //             email: values.email,
            //             password: values.password,
            //         },
            //     },
            // });
        },
        validationSchema: Yup.object().shape({
            email: Yup.string()
                .email('Must be a valid email')
                .max(255)
                .required('Email is required'),
            password: Yup.string().max(255).required('Password is required'),
        }),
    });
    return (
        <>
            <Head>
                <title>Вход</title>
            </Head>
            <Box
                display="flex"
                flexDirection="column"
                height="100%"
                justifyContent="center">
                <Container maxWidth="sm">
                    <form onSubmit={loginForm.handleSubmit}>
                        <Box mb={3}>
                            <Typography color="textPrimary" variant="h2">
                                Войти
                            </Typography>
                        </Box>
                        <Grid container spacing={3}>
                            <Grid item xs={12} md={6}>
                                <Button
                                    color="primary"
                                    fullWidth
                                    startIcon={<FacebookIcon />}
                                    // onClick={handleSubmit}
                                    size="large"
                                    variant="contained">
                                    Вход через Facebook
                                </Button>
                            </Grid>
                        </Grid>
                        <Box mt={3} mb={1}>
                            <Typography
                                align="center"
                                color="textSecondary"
                                variant="body1">
                                Войти с помощью Email
                            </Typography>
                        </Box>
                        <TextField
                            // error={Boolean(
                            //     touched.email && errors.email,
                            // )}
                            // helperText={touched.email && errors.email}
                            fullWidth
                            label="Email Address"
                            margin="normal"
                            name="email"
                            onBlur={loginForm.handleBlur}
                            onChange={loginForm.handleChange}
                            type="email"
                            value={loginForm.values.email}
                            variant="outlined"
                        />
                        <TextField
                            error={Boolean(
                                loginForm.touched.password &&
                                    loginForm.errors.password,
                            )}
                            fullWidth
                            helperText={
                                loginForm.touched.password &&
                                loginForm.errors.password
                            }
                            label="Password"
                            margin="normal"
                            name="password"
                            onBlur={loginForm.handleBlur}
                            onChange={loginForm.handleChange}
                            type="password"
                            value={loginForm.values.password}
                            variant="outlined"
                        />
                        <Box my={2}>
                            <Button
                                color="primary"
                                disabled={loginForm.isSubmitting}
                                fullWidth
                                size="large"
                                type="submit"
                                variant="contained">
                                Войти
                            </Button>
                        </Box>
                        <Typography color="textSecondary" variant="body1">
                            У вас нет аккаунта? &nbsp;
                            <Link href="/register">
                                <a>Регистрация</a>
                            </Link>
                        </Typography>
                    </form>
                </Container>
            </Box>
        </>
    );
};

export default Login;
