// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { NextApiRequest, NextApiResponse } from 'next';
import { ApolloServer } from 'apollo-server-micro';
import 'graphql-import-node';
import { buildContext } from 'graphql-passport';
import schema from '../../../server/graphql/admin';
import configDB from '../../../server/database';
import withPassport from '../../../server/lib/withPassport';

configDB();

const apolloServer = new ApolloServer({
    schema,
    context: ({ req, res }) => buildContext({ req, res }),
});

export const config = {
    api: {
        bodyParser: false,
    },
};

const handler = apolloServer.createHandler({ path: '/api/graphql-admin' });

const graphqlHandler = async (
    req: NextApiRequest,
    res: NextApiResponse,
): Promise<void> => {
    await handler(req, res);
};

export default withPassport(graphqlHandler);
