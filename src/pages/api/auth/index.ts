import { NextApiRequest, NextApiResponse } from 'next';

import withPassport, { passport } from '../../../../server/lib/withPassport';
import runMiddleware from '../../../../server/utils/runMiddleware';

const authenticate = runMiddleware(passport.authenticate('local'));

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    try {
        await authenticate(req, res);
        res.send(req.user);
    } catch (e) {
        res.send(e);
    }
};

export default withPassport(handler);
