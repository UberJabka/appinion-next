import { NextApiResponse, NextApiRequest } from 'next';
import withPassport, { passport } from '../../../../server/lib/withPassport';

const handler = (req: NextApiRequest, res: NextApiResponse) => {
    const { provider } = req.query;
    if (!provider) {
        res.send('hi');
    }
    passport.authenticate(provider)(req, res);
};

export default withPassport(handler);
