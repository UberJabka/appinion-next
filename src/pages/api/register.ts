import { NextApiRequest, NextApiResponse } from 'next';
import mongoose from 'mongoose';
import User, { IUser } from '../../../server/models/User';
import withPassport, { passport } from '../../../server/lib/withPassport';
import runMiddleware from '../../../server/utils/runMiddleware';

mongoose.connect(process.env.DB_HOST, {
    useNewUrlParser: true,
});

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    try {
        await User.register(
            {
                email: req.body.email as string,
                firstName: req.body.firstName as string,
                lastName: req.body.lastName as string,
            } as IUser,
            req.body.password,
        );
        await runMiddleware(passport.authenticate('local'))(req, res);

        res.send('awesome');
    } catch (e) {
        res.send(e);
    }
};

export default withPassport(handler);
