// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { NextApiRequest, NextApiResponse } from 'next';
import { ApolloServer } from 'apollo-server-micro';
import * as mongoose from 'mongoose';
import Cors from 'cors';
import runMiddleware from '../../../server/utils/runMiddleware';
import 'graphql-import-node';
import schema from '../../../server/graphql/schema';
import configDB from '../../../server/database';

configDB();

const apolloServer = new ApolloServer({
    schema,
    // resolvers,
});

export const config = {
    api: {
        bodyParser: false,
    },
};

const cors = runMiddleware(
    Cors({
        methods: ['POST'],
    }),
);

const handler = apolloServer.createHandler({ path: '/api/graphql' });

export default async function graphqlHandler(
    req: NextApiRequest,
    res: NextApiResponse,
): Promise<void> {
    await cors(req, res);
    await handler(req, res);
}
