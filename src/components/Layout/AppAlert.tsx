import { Alert, AlertTitle } from '@material-ui/lab';
import { Box } from '@material-ui/core';
import React from 'react';

interface Props {
    text: string;
    type: 'success' | 'error';
}

const AppAlert: React.FC<Props> = ({ text, type }) => {
    return (
        <Box mt={1} mb={1}>
            <Alert severity={type}>
                <AlertTitle>{text}</AlertTitle>
            </Alert>
        </Box>
    );
};
export default AppAlert;
