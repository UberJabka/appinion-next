import React, { useState } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
    Avatar,
    Box,
    Button,
    Card,
    Checkbox,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TablePagination,
    TableRow,
    Typography,
} from '@material-ui/core';
import styled from 'styled-components';
import LinkIcon from '@material-ui/icons/Link';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Review } from '../../graphql/generated/graphql';

type Props = {
    reviews: Review;
};
const AppAvatar = styled(Avatar)`
    margin-right: 15px;
`;

const ReviewsList: React.FC<Props> = ({ reviews }) => {
    // console.log(reviews.map((value) => console.log(value)));

    const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
    const [limit, setLimit] = useState(10);
    const [page, setPage] = useState(0);

    const router = useRouter();

    const handleSelectAll = (event) => {
        let newSelectedCustomerIds;

        // if (event.target.checked) {
        //     newSelectedCustomerIds = reviews.map((review) => review.id);
        // } else {
        //     newSelectedCustomerIds = [];
        // }

        setSelectedCustomerIds(newSelectedCustomerIds);
    };

    const handleSelectOne = (event, id) => {
        const selectedIndex = selectedCustomerIds.indexOf(id);
        let newSelectedCustomerIds = [];

        if (selectedIndex === -1) {
            newSelectedCustomerIds = newSelectedCustomerIds.concat(
                selectedCustomerIds,
                id,
            );
        } else if (selectedIndex === 0) {
            newSelectedCustomerIds = newSelectedCustomerIds.concat(
                selectedCustomerIds.slice(1),
            );
        } else if (selectedIndex === selectedCustomerIds.length - 1) {
            newSelectedCustomerIds = newSelectedCustomerIds.concat(
                selectedCustomerIds.slice(0, -1),
            );
        } else if (selectedIndex > 0) {
            newSelectedCustomerIds = newSelectedCustomerIds.concat(
                selectedCustomerIds.slice(0, selectedIndex),
                selectedCustomerIds.slice(selectedIndex + 1),
            );
        }

        setSelectedCustomerIds(newSelectedCustomerIds);
    };

    const handleLimitChange = (event) => {
        setLimit(event.target.value);
    };

    const handlePageChange = (event, newPage) => {
        setPage(newPage);
    };
    return (
        <Card>
            <PerfectScrollbar>
                <Box minWidth={1050}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell padding="checkbox">
                                    <Checkbox
                                        checked={
                                            selectedCustomerIds.length ===
                                            reviews?.length
                                        }
                                        color="primary"
                                        indeterminate={
                                            selectedCustomerIds.length > 0 &&
                                            selectedCustomerIds.length <
                                                reviews?.length
                                        }
                                        onChange={handleSelectAll}
                                    />
                                </TableCell>
                                <TableCell>Автор</TableCell>
                                <TableCell>Комментарий</TableCell>
                                <TableCell>Публикация</TableCell>
                                <TableCell>Дата создания</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {reviews?.map((review) => (
                                <TableRow hover key={review.id}>
                                    <TableCell padding="checkbox">
                                        <Checkbox
                                            checked={
                                                selectedCustomerIds.indexOf(
                                                    review.id,
                                                ) !== -1
                                            }
                                            onChange={(event) =>
                                                handleSelectOne(
                                                    event,
                                                    review.id,
                                                )
                                            }
                                            value="true"
                                        />
                                    </TableCell>
                                    <TableCell>
                                        <Box alignItems="center" display="flex">
                                            <AppAvatar
                                                src={review.author.photo.url}>
                                                {review.author.name}
                                            </AppAvatar>
                                            <Typography
                                                color="textPrimary"
                                                variant="body1">
                                                {review.author.name}
                                            </Typography>
                                        </Box>
                                    </TableCell>
                                    <TableCell>{review.value}</TableCell>
                                    <TableCell>Опубликовано</TableCell>
                                    <TableCell>01.01.2020</TableCell>
                                    <TableCell>
                                        <Link
                                            href="/admin/shops/[id]/[review]"
                                            as={`/admin/shops/${router.query.id}/${review.id}`}>
                                            <LinkIcon />
                                        </Link>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Box>
            </PerfectScrollbar>
            {/* <TablePagination */}
            {/*    component="div" */}
            {/*    // count={reviews.length} */}
            {/*    onChangePage={handlePageChange} */}
            {/*    onChangeRowsPerPage={handleLimitChange} */}
            {/*    page={page} */}
            {/*    rowsPerPage={limit} */}
            {/*    rowsPerPageOptions={[5, 10, 25]} */}
            {/* /> */}
        </Card>
    );
};

export default ReviewsList;
