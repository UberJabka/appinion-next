import React from 'react';
import { AppBar, Badge, Hidden, IconButton, Toolbar } from '@material-ui/core';
import Link from 'next/link';
import NotificationsIcon from '@material-ui/icons/NotificationsOutlined';
import InputIcon from '@material-ui/icons/Input';
import MenuIcon from '@material-ui/icons/Menu';
import styled from 'styled-components';
import { useLogoutMutation } from '../../graphql/generated/graphql';

const Logo = styled.span`
    display: inline-block;
    cursor: pointer;
`;

const TopBar: React.FC = () => {
    return (
        <AppBar>
            <Toolbar>
                <>
                    <Link href="/">
                        <Logo>Appinion</Logo>
                    </Link>
                </>
                <Hidden mdDown>
                    <IconButton color="inherit">
                        <Badge color="primary" variant="dot">
                            <NotificationsIcon />
                        </Badge>
                    </IconButton>
                    <IconButton color="inherit">
                        <InputIcon />
                    </IconButton>
                </Hidden>
                <Hidden lgUp>
                    <IconButton color="inherit">
                        <MenuIcon />
                    </IconButton>
                </Hidden>
            </Toolbar>
        </AppBar>
    );
};

export default TopBar;
