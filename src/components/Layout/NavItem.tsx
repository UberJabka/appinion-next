import React from 'react';
import { Button, ListItem } from '@material-ui/core';
import styled from 'styled-components';
import Link from 'next/link';

type Props = {
    title: string;
    href: string;
    Icon: React.FC;
};
const Title = styled.span`
    margin-right: auto;
    text-decoration: none;
    margin-left: 5px;
`;
const AppButton = styled(Button)`
    justify-content: flex-start;
    letter-spacing: 0;
    padding: 10px 8px;
    text-transform: none;
    width: 100%;
`;

const NavItem: React.FC<Props> = ({ title, href, Icon }) => {
    return (
        <ListItem>
            <Link href={href}>
                <AppButton>
                    <Icon />
                    <Title>{title}</Title>
                </AppButton>
            </Link>
        </ListItem>
    );
};
export default NavItem;
