import { CircularProgress } from '@material-ui/core';
import React from 'react';
import styled from 'styled-components';

const LoaderWrap = styled.div`
    display: flex;
    height: 100%;
    width: 100%;
    justify-content: center;
    align-items: center;
`;

const Loader: React.FC = () => {
    return (
        <LoaderWrap>
            <CircularProgress />
        </LoaderWrap>
    );
};

export default Loader;
