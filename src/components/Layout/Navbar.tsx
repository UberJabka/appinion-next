import React from 'react';
import {
    Avatar,
    Box,
    Divider,
    Drawer,
    Hidden,
    List,
    Typography,
} from '@material-ui/core';
import {
    ShoppingBag as ShoppingBagIcon,
    BarChart as BarChartIcon,
    Lock as LockIcon,
    Settings as SettingsIcon,
    User as UserIcon,
    UserPlus as UserPlusIcon,
} from 'react-feather';
import styled from 'styled-components';
import Link from 'next/link';
import NavItem from './NavItem';
import useAuth from '../../containers/AuthProvider';
import { User } from '../../graphql/generated/graphql';

const items = [
    {
        href: '/admin',
        icon: BarChartIcon,
        title: 'Dashboard',
    },
    {
        href: '/admin/shops',
        icon: ShoppingBagIcon,
        title: 'Магазины',
    },
    {
        href: '/admin/account',
        icon: UserIcon,
        title: 'Аккаунт',
    },
    {
        href: '/admin/settings',
        icon: SettingsIcon,
        title: 'Настройки',
    },
    {
        href: '/login',
        icon: LockIcon,
        title: 'Login',
    },
    {
        href: '/register',
        icon: UserPlusIcon,
        title: 'Register',
    },
];

const MobileDrawer = styled(Drawer)`
    & .MuiDrawer-paper {
        width: 256px;
    }
`;
const DesktopDrawer = styled(Drawer)`
    & .MuiDrawer-paper {
        width: 256px;
        top: 64px;
        height: calc(100% - 64px);
    }
`;
type Props = {
    user: User;
};

const Content: React.FC<Props> = ({ user }) => {
    if (user) {
        return (
            <>
                <Box
                    alignItems="center"
                    display="flex"
                    flexDirection="column"
                    p={2}>
                    <Link href="/administrator/account">
                        <a>
                            <Avatar src={user?.photo.url} />
                        </a>
                    </Link>
                    <Typography color="textPrimary" variant="h5">
                        {`${user?.firstName} ${user?.lastName}`}
                    </Typography>
                    <Typography color="textSecondary" variant="body2">
                        Тариф
                    </Typography>
                </Box>
                <Divider />
                <Box p={2}>
                    <List>
                        {items.map((item) => (
                            <NavItem
                                href={item.href}
                                key={item.title}
                                title={item.title}
                                Icon={item.icon}
                            />
                        ))}
                    </List>
                </Box>
            </>
        );
    }
    return <></>;
};

const NavBar: React.FC = () => {
    const { user } = useAuth();
    return (
        <>
            <Hidden lgUp>
                <MobileDrawer anchor="left" variant="temporary">
                    <Content user={user} />
                </MobileDrawer>
            </Hidden>
            <Hidden mdDown>
                <DesktopDrawer anchor="left" open variant="persistent">
                    <Content user={user} />
                </DesktopDrawer>
            </Hidden>
        </>
    );
};
export default NavBar;
