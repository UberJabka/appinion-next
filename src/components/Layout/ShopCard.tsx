import React, { useState } from 'react';
import {
    Card,
    CardContent,
    Box,
    Avatar,
    Typography,
    Grid,
    Divider,
    Dialog,
    DialogContent,
    DialogContentText,
    DialogActions,
    Button,
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import styled from 'styled-components';
import { MutationUpdaterFn } from '@apollo/client/core';
import Link from 'next/link';
import {
    GetShopsDocument,
    Shop,
    useRemoveShopMutation,
    RemoveShopMutation,
} from '../../graphql/generated/graphql';

type Props = {
    shop: Shop;
};

const AppAvatar = styled(Avatar)`
    height: 80px !important;
    width: 80px !important;
    display: flex;
    justify-content: center;
    align-items: center;
`;

const AppGrid = styled(Grid)`
    width: 100%;
    align-items: center;
    justify-content: space-between;
    display: flex;
`;
const ModalText = styled.h4`
    text-align: center;
`;
const PreviewText = styled.div`
    display: flex;
    min-height: 50px;
    align-items: center;
    justify-content: center;
`;
const ShopCard: React.FC<Props> = ({ shop }) => {
    const [
        removeShopMutation,
        { data, loading, error, client },
    ] = useRemoveShopMutation();

    function updateCache(clientParam): MutationUpdaterFn<RemoveShopMutation> {
        const dataCache = clientParam.readQuery({
            query: GetShopsDocument,
        });
        const newData = {
            shops: dataCache.shops.filter((t) => t.id !== shop.id),
        };
        return clientParam.writeQuery({
            query: GetShopsDocument,
            data: newData,
        });
    }

    const [open, setOpen] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    return (
        <>
            <Card>
                <CardContent>
                    <Box display="flex" justifyContent="center" mb={3}>
                        {shop?.photo?.url ? (
                            <AppAvatar
                                alt="Product"
                                src={shop?.photo?.url}
                                variant="square"
                            />
                        ) : (
                            <AppAvatar>{shop.name.substring(0, 1)}</AppAvatar>
                        )}
                    </Box>
                    <Typography
                        align="center"
                        color="textPrimary"
                        gutterBottom
                        variant="h4">
                        {shop.name}
                    </Typography>
                    <PreviewText>
                        {shop?.desc && (
                            <Typography
                                align="center"
                                color="textPrimary"
                                variant="body1">
                                {shop?.desc.length > 74
                                    ? `${shop?.desc.substring(0, 74)}...`
                                    : shop?.desc}
                            </Typography>
                        )}
                    </PreviewText>
                </CardContent>
                <Box flexGrow={1} />
                <Divider />
                <Box p={2}>
                    <Grid container justify="space-between" spacing={2}>
                        <AppGrid item>
                            <Link
                                href="/admin/shops/[id]"
                                as={`/admin/shops/${shop.id}`}>
                                <Button color="primary" variant="contained">
                                    Подробнее
                                </Button>
                            </Link>
                            <DeleteIcon
                                color="action"
                                onClick={handleClickOpen}
                            />
                        </AppGrid>
                    </Grid>
                </Box>
            </Card>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Вы Уверены что хотите удалить магазин
                        <ModalText>{` ${shop?.name} ?`}</ModalText>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button
                        onClick={() =>
                            removeShopMutation({
                                variables: {
                                    input: {
                                        id: shop?.id,
                                    },
                                },
                                update: updateCache(client),
                            })
                        }
                        variant="contained"
                        color="secondary">
                        Удалить
                    </Button>
                    <Button onClick={handleClose} color="primary" autoFocus>
                        Отмена
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};
export default ShopCard;
